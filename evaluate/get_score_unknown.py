from evaluate.getFeatures import getFeatures
from pymongo import MongoClient
from evaluate._utils import DataGenerator
from utils.ops import load
import sys
from tqdm import tqdm
import tensorflow as tf
import tensorlayer as tl
from other_algos.anv_v6_tf_api import anvv6 as anv_v6_api
# from facenet_api import facenet_api
import cv2
import os
import numpy as np
import logging.config
# from face_reco_models.face_recognition.api import face_encodings

from configparser import ConfigParser

parser = ConfigParser()

# Change this with absolute path if in a different location
parser.read('../config.ini')
logging.config.fileConfig('../logging.ini')

client = MongoClient('localhost', 27017)
db = client.score_db
input_ = ''
# while input_ not in ['y', 'n', 'Y', 'N']:
#     input_ = input("Do you really want to delete collection score_collection_0_99? [y/n]")
#     if input_ in ['y', 'Y']:
#         db.score_collection_0_99.drop()
db.score_collection_0_99.drop()
score_collection = db.score_collection_0_99
score_file = {'_id': '',
              'class': None,
              'score_original': [],
              'score_degraded': [],
              'score_enhanced': []}

gallery_path = parser.get('TESTING', 'gallery_path')
probe_path_degraded = parser.get('TESTING', 'test_data_path')
probe_path_enhanced = parser.get('TESTING', 'enhanced_data_path')
num_folders = int(parser.get('MONGO', 'num_folders'))
algorithm = parser.get('MONGO', 'algo')
gt_features_file = parser.get('DATA', 'gt_features')

img_placeholder = tf.placeholder(dtype=tf.float32, shape=(None, 128, 128, 3))
sess = tf.Session()

if algorithm == 'anvv6':
    feature_length = 256
    net = anv_v6_api(img_placeholder, reuse=tf.AUTO_REUSE)
    model_weights = np.load('/mnt/sda2/soumya/PhD@AnyVision/enhanceface/v6pytorch/anv_v6.weights.npy',
                            encoding='latin1')
    tl.files.assign_params(sess, model_weights, net)
    sim_dist = 'cosine'
# elif algorithm == 'facenet':
#     feature_length = 512
#     sess.run(tf.global_variables_initializer())
#     net = facenet_api(img_placeholder, reuse=tf.AUTO_REUSE)
#     model_path = os.path.join('face_reco_models/facenet_model', 'model-20180402-114759.ckpt-275')
#     load(sess, model_path)
#     sim_dist = 'euclidean'
# elif algorithm == 'dlib':
#     feature_length = 128
#     sess = None
#     img_placeholder = None
#     class net:
#         def __init__(self):
#             self.outputs = None
#     net = net()
#     net.outputs = face_encodings
#     sim_dist = 'euclidean'

gen = DataGenerator(data_path=probe_path_degraded)

try:
    gt_features_all = np.load(gt_features_file)
except IOError:
    gt_features_all = np.zeros(shape=(num_folders, feature_length))
    for folders in os.listdir(gallery_path):
        class_n = int(os.path.basename(folders))
        folders = os.path.join(gallery_path, folders)
        try:
            gt_path = os.path.join(folders, os.listdir(folders)[0])
            if os.path.exists(gt_path):
                image = cv2.resize(cv2.imread(gt_path), (128, 128)) / 255.0
                image = np.expand_dims(image, axis=0)
                feature = getFeatures(sess, net.outputs, image, img_placeholder)
                gt_features_all[class_n, :] = feature
        except IndexError:
            pass

    np.save(gt_features_file, gt_features_all)

norm_all_gt = np.linalg.norm(gt_features_all, axis=1)
nan_score_list = [np.nan] * num_folders


def compute_score(ori_features, gt_features_all, type='cosine'):
    if type == 'cosine':
        dot_pdt = np.dot(ori_features, gt_features_all.T)
        norm_pdt = np.expand_dims(np.linalg.norm(ori_features, axis=1), 1) * norm_all_gt
        score_array = dot_pdt / norm_pdt
    elif type == 'euclidean':
        score_array = np.expand_dims(np.linalg.norm(ori_features - gt_features_all, axis=1), axis=0)
    score_list = score_array.tolist()
    return score_list


def get_score_(batch=2):
    t = tqdm(total=5288)
    batch_deg = np.zeros(shape=(batch, 128, 128, 3))
    batch_enh = np.zeros(shape=(batch, 128, 128, 3))
    batch_fname = []
    batch_gt_class = []
    batch_counter = 0
    for image_deg, image_enh, filename in gen.yield_data_for_score_no_original(probe_path_degraded,
                                                                               probe_path_enhanced, extension='png'):
        gt_class = os.path.dirname(filename)
        if image_deg is None or image_enh is None:
            score_deg = nan_score_list
            score_enh = nan_score_list
            score_file['_id'] = filename  # this has to be unique for every document
            score_file['class'] = gt_class
            score_file['score_degraded'] = score_deg
            score_file['score_enhanced'] = score_enh
            score_collection.insert_one(score_file)
            t.update(n=batch)
        else:
            batch_deg[batch_counter, :, :, :] = image_deg / 255.0
            batch_enh[batch_counter, :, :, :] = image_enh / 255.0
            batch_fname.append(filename)
            batch_gt_class.append(gt_class)
            batch_counter += 1
        if batch_counter == batch:
            deg_features = getFeatures(sess, net.outputs, batch_deg, img_placeholder)
            enh_features = getFeatures(sess, net.outputs, batch_enh, img_placeholder)
            score_deg = compute_score(deg_features, gt_features_all, type=sim_dist)
            score_enh = compute_score(enh_features, gt_features_all, type=sim_dist)
            for i in range(batch):
                score_file['_id'] = batch_fname[i]  # this has to be unique for every document
                score_file['class'] = batch_gt_class[i]
                score_file['score_degraded'] = score_deg[i]
                score_file['score_enhanced'] = score_enh[i]
                score_collection.insert_one(score_file)
            t.update(n=batch)
            batch_counter = 0
            batch_deg = np.zeros(shape=(batch, 128, 128, 3))
            batch_enh = np.zeros(shape=(batch, 128, 128, 3))
            batch_fname = []
            batch_gt_class = []
    t.close()


if __name__ == '__main__':
    get_score_(1)
