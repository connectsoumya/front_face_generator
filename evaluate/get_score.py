from evaluate.getFeatures import getFeatures
from pymongo import MongoClient
from evaluate._utils import DataGenerator
import sys
from tqdm import tqdm, trange
import tensorflow as tf
import tensorlayer as tl
from other_algos.anv_v6_tf_api import anvv6 as anv_v6_api
import cv2

# sys.path.append('/home/soumya/Downloads/caffe/python')
# import caffe
import os
import numpy as np

import logging.config

from configparser import ConfigParser

parser = ConfigParser()

# Change this with absolute path if in a different location
parser.read('config.ini')
logging.config.fileConfig('logging.ini')
class_gt_path = parser.get('FaceRecognition', 'face_gt')
original_image_path = parser.get('FaceRecognition', 'face_data_original')
degraded_image_path = parser.get('FaceRecognition', 'face_data_degraded')
enhanced_image_path = parser.get('FaceRecognition', 'face_data_enhanced')
# net = caffe.Net("caffe_model/v6_bgr.prototxt", "caffe_model/v6_bgr.caffemodel", caffe.TEST)
feature_length = 256

client = MongoClient('localhost', 27017)
db = client.score_db
input_ = ''
while input_ not in ['y', 'n', 'Y', 'N']:
    input_ = raw_input("Do you really want to delete collection score_collection_0_99? [y/n]")
    if input_ in ['y', 'Y']:
        db.score_collection_0_99.drop()
score_collection = db.score_collection_0_99
score_file = {'_id': '',
              'class': None,
              'score_original': [],
              'score_degraded': [],
              'score_enhanced': []}

data_path = original_image_path
start_folder = int(parser.get('DataGeneratorAR', 'test_data_start_folder'))
end_folder = int(parser.get('DataGeneratorAR', 'test_data_end_folder'))
gen = DataGenerator(data_path=data_path)
img1 = tf.placeholder(dtype=tf.float32, shape=(None, 128, 128, 3))
net = anv_v6_api(img1, reuse=tf.AUTO_REUSE)
params_val = np.load('/mnt/sda2/soumya/PhD@AnyVision/enhanceface/v6pytorch/anv_v6.weights.npy')
sess = tf.Session()
tl.files.assign_params(sess, params_val, net)
try:
    # gt_features_all = np.load('gt_features_%d_%d.npy' % (start_folder, end_folder))
    gt_features_all = np.load('gt_features_0_100.npy')
except IOError:
    gt_features_all = np.zeros(shape=(end_folder - start_folder + 1, feature_length))
    counter = 0
    for i in trange(start_folder, end_folder + 1):
        gt_path = os.path.join(class_gt_path, '%.5d/gt.jpg' % i)
        if os.path.exists(gt_path):
            image = cv2.resize(cv2.imread(gt_path), (128, 128))
            image = np.expand_dims(image, axis=0)
            feature = getFeatures(sess, net, image, img1)
            gt_features_all[counter, :] = feature
        else:
            gt_features_all[counter, :] = np.array([np.nan] * 256)
        counter += 1
    np.save('gt_features_%d_%d.npy' % (start_folder, end_folder), gt_features_all)

norm_all_gt = np.linalg.norm(gt_features_all, axis=1)
nan_score_list = [np.nan] * (end_folder - start_folder - 1)


def compute_score(ori_features, gt_features_all):
    dot_pdt = np.dot(ori_features, gt_features_all.T)
    norm_pdt = np.expand_dims(np.linalg.norm(ori_features, axis=1), 1) * norm_all_gt
    score_array = dot_pdt / norm_pdt
    score_list = score_array.tolist()
    return score_list


def get_score_(batch=2):
    t = tqdm(total=9892)
    batch_ori = np.zeros(shape=(batch, 128, 128, 3))
    batch_deg = np.zeros(shape=(batch, 128, 128, 3))
    batch_enh = np.zeros(shape=(batch, 128, 128, 3))
    batch_fname = []
    batch_gt_class = []
    batch_counter = 0
    for image_ori, image_deg, image_enh, filename in gen.yield_data_for_score(start_folder, end_folder):
        gt_class = os.path.dirname(filename)
        if image_ori is None or image_deg is None or image_enh is None:
            score_ori = nan_score_list
            score_deg = nan_score_list
            score_enh = nan_score_list
            score_file['_id'] = filename  # this has to be unique for every document
            score_file['class'] = gt_class
            score_file['score_original'] = score_ori
            score_file['score_degraded'] = score_deg
            score_file['score_enhanced'] = score_enh
            score_collection.insert_one(score_file)
            t.update(n=batch)
        else:
            batch_ori[batch_counter, :, :, :] = image_ori
            batch_deg[batch_counter, :, :, :] = image_deg
            batch_enh[batch_counter, :, :, :] = image_enh
            batch_fname.append(filename)
            batch_gt_class.append(gt_class)
            batch_counter += 1
        if batch_counter == batch:
            ori_features = getFeatures(sess, net, batch_ori, img1)
            deg_features = getFeatures(sess, net, batch_deg, img1)
            enh_features = getFeatures(sess, net, batch_enh, img1)
            score_ori = compute_score(ori_features, gt_features_all)
            score_deg = compute_score(deg_features, gt_features_all)
            score_enh = compute_score(enh_features, gt_features_all)
            for i in range(batch):
                score_file['_id'] = batch_fname[i]  # this has to be unique for every document
                score_file['class'] = batch_gt_class[i]
                score_file['score_original'] = score_ori[i]
                score_file['score_degraded'] = score_deg[i]
                score_file['score_enhanced'] = score_enh[i]
                score_collection.insert_one(score_file)
            t.update(n=batch)
            batch_counter = 0
            batch_ori = np.zeros(shape=(batch, 128, 128, 3))
            batch_deg = np.zeros(shape=(batch, 128, 128, 3))
            batch_enh = np.zeros(shape=(batch, 128, 128, 3))
            batch_fname = []
            batch_gt_class = []
    t.close()


if __name__ == '__main__':
    get_score_(2)
