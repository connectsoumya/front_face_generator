from pymongo import MongoClient
import sys
import matplotlib.pyplot as plt
import os
import numpy as np
from numpy import trapz
from os.path import basename, normpath

import logging
import logging.config

from configparser import ConfigParser

parser = ConfigParser()

# Change this with absolute path if in a different location
parser.read('../config.ini')
logging.config.fileConfig('../logging.ini')
client = MongoClient('localhost', 27017)
db = client.score_db
enh_im_path = parser.get('TESTING', 'enhanced_data_path')
algorithm = parser.get('MONGO', 'algo')
plot_data_filename = basename(normpath(enh_im_path + '..')) + basename(normpath(enh_im_path))
plot_oridata_filename = basename(normpath(enh_im_path))


def scores(tp, fp, tn, fn):
    tpr = recall = tp / (tp + fn)
    tnr = tn / (tn + fp)
    fpr = fp / (tn + fp)
    fnr = fn / (tp + fn)
    precision = tp / (tp + fp)
    score = {'TPR': tpr,
             'TNR': tnr,
             'FPR': fpr,
             'FNR': fnr,
             'Precision': precision,
             'Recall': recall}
    return score


def fetch_scores(threshold, probe):
    if probe not in ['degraded', 'enhanced']:
        raise ValueError('Probe must be one of these: original, degraded, enhanced')
    probe = 'score_' + probe
    tp = 0.0
    fp = 0.0
    fn = 0.0
    tn = 0.0
    for doc in db.score_collection_0_99.find():
        class_id = int(doc['class'])
        score_list = doc[probe]
        if algorithm == 'anvv6':
            above_threshold = np.where((np.array(score_list)) >= threshold)[0].tolist()
            below_threshold = np.where((np.array(score_list)) < threshold)[0].tolist()
        if class_id in above_threshold:
            tp += 1
            above_threshold.remove(class_id)
        else:  # class_id in below_threshold:
            if os.path.exists(os.path.join(enh_im_path, '%.3d' % class_id)) or os.path.exists(
                    os.path.join(enh_im_path, '%.5d' % class_id)):
                fn += 1
        if class_id in below_threshold:
            below_threshold.remove(class_id)
        fp += len(above_threshold)
        tn += len(below_threshold)
    score = scores(tp, fp, tn, fn)
    return score['FPR'], score['TPR']


if __name__ == '__main__':
    plot_data_deg = []
    plot_data_enh = []
    points = 100
    y_vals_deg = []
    y_vals_enh = []
    f1_vals_deg = []
    f1_vals_enh = []
    ticks = np.linspace(0.0, 0.5, points)
    for i in ticks:
        pr_re_deg = fetch_scores(i, 'degraded')
        pr_re_enh = fetch_scores(i, 'enhanced')
        cond_deg = pr_re_deg[0] == 0 and pr_re_deg[1] == 0
        cond_enh = pr_re_enh[0] == 0 and pr_re_enh[1] == 0
        if not cond_deg:
            plot_data_deg.append(pr_re_deg)
            y_vals_deg.append(pr_re_deg[0])
        if not cond_enh:
            plot_data_enh.append(pr_re_enh)
            y_vals_enh.append(pr_re_enh[0])
        if (cond_deg and cond_enh) or (pr_re_deg[0] == 0 and pr_re_enh[0] == 0):
            break
        # t.update(n=1)
        try:
            f1_score_deg = (pr_re_deg[0] * pr_re_deg[1]) / (pr_re_deg[0] + pr_re_deg[1])
            f1_score_enh = (pr_re_enh[0] * pr_re_enh[1]) / (pr_re_enh[0] + pr_re_enh[1])
        except ZeroDivisionError:
            f1_score_deg = f1_vals_deg[-1]
            f1_score_enh = f1_vals_enh[-1]
        f1_vals_deg.append(f1_score_deg)
        f1_vals_enh.append(f1_score_enh)
        print(i, '|', pr_re_deg[0], pr_re_deg[1], '|', pr_re_enh[0], pr_re_enh[1])
    # t.close()
    auc_deg = trapz(y_vals_deg, dx=ticks[10] - ticks[9])
    auc_enh = trapz(y_vals_enh, dx=ticks[10] - ticks[9])
    plot_data_deg = list(zip(*plot_data_deg))
    plot_data_enh = list(zip(*plot_data_enh))
    plt.plot(plot_data_enh[0], plot_data_enh[1], 'g', linewidth=0.5)
    plt.plot(plot_data_deg[0], plot_data_deg[1], 'b', linewidth=0.5)
    plt.grid(color='k', linestyle='-', linewidth=0.25)
    plt.legend(('Enhanced', 'Degraded'))
    plt.xlabel('Precision, AUC: D: %0.3f, E: %0.3f' % (auc_deg, auc_enh))
    plt.ylabel('Recall')
    plt.show()
    plt.savefig('ROC_Graph', dpi=None, facecolor='w', edgecolor='w',
                orientation='portrait', papertype=None, format=None,
                transparent=False, bbox_inches=None, pad_inches=0.1,
                frameon=None)
