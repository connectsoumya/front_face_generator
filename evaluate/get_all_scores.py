# coding: utf-8
import mxnet as mx
from mini_pipeline.mtcnn_detector import MtcnnDetector
import numpy as np
import cv2
import os
import time
from sklearn.metrics.pairwise import cosine_similarity
from shutil import copyfile

from mini_pipeline.zen_rescnn_cm import ZenResCNNCMdeploy
import torch
from torch.autograd import Variable
import glob


def prepare_img(img, target_size=128):
    yy = int((img.shape[0] - target_size) / 2)
    xx = int((img.shape[1] - target_size) / 2)
    resized_img = img[yy: yy + target_size, xx: xx + target_size]

    result = np.zeros((1, 3, target_size, target_size), dtype=np.float32)

    # RGB data within [0,1]
    result[0, 2, :, :] = resized_img[:, :, 0]
    result[0, 1, :, :] = resized_img[:, :, 1]
    result[0, 0, :, :] = resized_img[:, :, 2]
    result = result / 255.0

    result = torch.from_numpy(result)
    result = Variable(result).cuda()

    return result


def load_weight(model, state_dict):
    '''
        load weight from state_dict to model, skip those with invalid shape
    :param model:
    :param state_dict:
    :return:
    '''
    for name, param in model.named_parameters():
        if not name in state_dict:
            continue
        if param.size() == state_dict[name].size():
            print('==> loading weight from ', name)
            param.data.copy_(state_dict[name])


# load detector
detector = MtcnnDetector(model_folder='../mini_pipeline/model', ctx=mx.gpu(0), num_worker=1, accurate_landmark=True)

# load feature extractor
checkpoint_file = '../mini_pipeline/zencnn_lm.pth'
model = ZenResCNNCMdeploy()
model.eval()
model.cuda()

# load state_dict
sd = torch.load(checkpoint_file)
sd = sd['state_dict']
sd = {k.partition('module.')[2]: v for k, v in sd.items()}
load_weight(model, sd)
print('-> FR model loaded')


# detect and align images
# enhanced
# img1 = cv2.imread('/media/soumya/HDD_1/testing_subset/to_test/Original')
# img2 = cv2.imread('/media/soumya/HDD_1/testing_subset/to_test/ARCNN')
# img3 = cv2.imread('/media/soumya/HDD_1/testing_subset/to_test/IEGAN')
# img4 = cv2.imread('/media/soumya/HDD_1/testing_subset/to_test/IRCNN')
# img5 = cv2.imread('/media/soumya/HDD_1/testing_subset/to_test/Ours')
#
# img = cv2.imread('/media/soumya/HDD_1/Face_Datasets/scface/cropped/gallery/022/022_frontal.png')


def get_features(path):
    img = cv2.imread(path)
    results = detector.detect_face(img)
    if results is not None:
        total_boxes, points = results
        chips = detector.extract_image_chips(img, points, 144, 0.37)
        chip = chips[0]
        if chip is None:
            print('can not detect face')
            return
    else:
        return
    in_data = prepare_img(chip, target_size=128)
    f = model(in_data).cpu().data.numpy()
    return f


for foldr in glob.glob('/media/soumya/HDD_1/Face_Datasets/scface/cropped/probe/' + '*/'):
    classname = foldr.split('/')[-2]
    gt_path = '/media/soumya/HDD_1/Face_Datasets/scface/cropped/gallery/' + classname + '/' + classname + '_frontal.png'
    gt_feat = get_features(gt_path)
    for or_path in glob.glob(foldr + '*.png'):
        filename = or_path.split('/')[-1]
        ar_path = '/media/soumya/HDD_1/testing_subset/to_test/ARCNN/' + classname + '/' + filename
        ie_path = '/media/soumya/HDD_1/testing_subset/to_test/IEGAN/' + classname + '/' + filename
        ir_path = '/media/soumya/HDD_1/testing_subset/to_test/IRCNN/' + classname + '/' + filename
        our_path = '/media/soumya/HDD_1/testing_subset/to_test/enh/' + classname + '/' + filename

        or_feat = get_features(or_path)
        ar_feat = get_features(ar_path)
        ie_feat = get_features(ie_path)
        ir_feat = get_features(ir_path)
        our_feat = get_features(our_path)

        score_or = cosine_similarity(gt_feat, or_feat)[0][0]
        score_ar = cosine_similarity(gt_feat, ar_feat)[0][0]
        score_ie = cosine_similarity(gt_feat, ie_feat)[0][0]
        score_ir = cosine_similarity(gt_feat, ir_feat)[0][0]
        score_our = cosine_similarity(gt_feat, our_feat)[0][0]
        if score_our > max(score_or, score_ar, score_ie, score_ir) + 0.1 and score_our < 0.9:
            print(
                f'{filename:10s}: {score_or:0.4f}, {score_ar:0.4f}, {score_ie:0.4f}, {score_ir:0.4f}, {score_our:0.4f}')
            try:
                copyfile(or_path, '/home/soumya/Videos/supp/Original/' + filename)
                copyfile(ar_path, '/home/soumya/Videos/supp/ARCNN/' + filename)
                copyfile(ie_path, '/home/soumya/Videos/supp/IEGAN/' + filename)
                copyfile(ir_path, '/home/soumya/Videos/supp/IRCNN/' + filename)
                copyfile(our_path, '/home/soumya/Videos/supp/Ours/' + filename)
                copyfile(gt_path, '/home/soumya/Videos/supp/GT/' + classname + '_frontal.png')
            except:
                continue
