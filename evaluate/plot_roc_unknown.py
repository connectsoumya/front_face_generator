from pymongo import MongoClient
import sys
import matplotlib.pyplot as plt
import os
import numpy as np
from numpy import trapz
from os.path import basename, normpath

import logging
import logging.config

from configparser import ConfigParser
parser = ConfigParser()

# Change this with absolute path if in a different location
parser.read('../config.ini')
logging.config.fileConfig('../logging.ini')
client = MongoClient('localhost', 27017)
db = client.score_db
enh_im_path = parser.get('TESTING', 'enhanced_data_path')
algorithm = parser.get('MONGO', 'algo')
plot_data_filename = basename(normpath(enh_im_path + '..')) + basename(normpath(enh_im_path))
plot_oridata_filename = basename(normpath(enh_im_path))


def fetch_precision_recall(threshold, probe):
    if probe not in ['degraded', 'enhanced']:
        raise ValueError('Probe must be one of these: original, degraded, enhanced')
    probe = 'score_' + probe
    tp = 0.0
    fp = 0.0
    fn = 0.0
    tn = 0.0
    # true = 0
    # false = 0
    for doc in db.score_collection_0_99.find():
        class_id = int(doc['class'])
        score_list = doc[probe]
        if algorithm == 'anvv6':
            above_threshold = np.where((np.array(score_list)) >= threshold)[0].tolist()
            below_threshold = np.where((np.array(score_list)) < threshold)[0].tolist()
        # elif algorithm == 'dlib':
        #     # for dlib similarity metric is euclidean. so its the opposite of anv6
        #     above_threshold = np.where(np.array(score_list) <= threshold)[0].tolist()
        #     below_threshold = np.where((np.array(score_list)) > threshold)[0].tolist()
        if class_id in above_threshold:
            tp += 1
            above_threshold.remove(class_id)
        elif class_id in below_threshold:
            if os.path.exists(os.path.join(enh_im_path, '%.3d' % class_id)) or os.path.exists(
                    os.path.join(enh_im_path, '%.7d' % class_id)):
                fn += 1
        # else:# class_id in below_threshold:
        #     if os.path.exists(os.path.join(enh_im_path, '%.3d' % class_id)) or os.path.exists(
        #             os.path.join(enh_im_path, '%.7d' % class_id)):
        #         fn += 1
        if class_id in below_threshold:
            below_threshold.remove(class_id)
        fp += len(above_threshold)
        tn += len(below_threshold)
        # if score_list[class_id] == np.min(score_list):
        #     true += 1
        # else:
        #     false += 1
    # print true, false
    try:
        tpr = tp / (tp + fn)
    except ZeroDivisionError:
        tpr = 0
    try:
        fpr = fp / (fp + tn)
    except ZeroDivisionError:
        fpr = 0
    return fpr, tpr


if __name__ == '__main__':
    plot_data_deg = []
    plot_data_enh = []
    points = 100
    # t = tqdm(total=points)
    y_vals_deg = []
    y_vals_enh = []
    x_vals_deg = []
    x_vals_enh = []
    f1_vals_deg = []
    f1_vals_enh = []
    ticks = np.linspace(0, 0.3, points)
    for i in ticks:
        pr_re_deg = fetch_precision_recall(i, 'degraded')
        pr_re_enh = fetch_precision_recall(i, 'enhanced')
        cond_deg = pr_re_deg[0] == 0 and pr_re_deg[1] == 0
        cond_enh = pr_re_enh[0] == 0 and pr_re_enh[1] == 0
        if not cond_deg:
            plot_data_deg.append(pr_re_deg)
            y_vals_deg.append(pr_re_deg[0])
            x_vals_deg.append(pr_re_deg[1])
        if not cond_enh:
            plot_data_enh.append(pr_re_enh)
            y_vals_enh.append(pr_re_enh[0])
            x_vals_enh.append(pr_re_deg[1])
        # if (cond_deg and cond_enh) or (pr_re_deg[0] == 0 and pr_re_enh[0] == 0):
        #     break
        # t.update(n=1)
        try:
            f1_score_deg = (pr_re_deg[0] * pr_re_deg[1]) / (pr_re_deg[0] + pr_re_deg[1])
            f1_score_enh = (pr_re_enh[0] * pr_re_enh[1]) / (pr_re_enh[0] + pr_re_enh[1])
        except ZeroDivisionError:
            f1_score_deg = f1_vals_deg[-1]
            f1_score_enh = f1_vals_enh[-1]
        f1_vals_deg.append(f1_score_deg)
        f1_vals_enh.append(f1_score_enh)
        print(i, '|', pr_re_deg[0], pr_re_deg[1], '|', pr_re_enh[0], pr_re_enh[1])
    # t.close()
    dx = (ticks[1] - ticks[0]) / 2
    auc_deg = np.mean(np.array(x_vals_deg) * np.array(y_vals_deg))
    auc_enh = np.mean(np.array(x_vals_enh) * np.array(y_vals_enh))
    plot_data_deg = list(zip(*plot_data_deg))
    plot_data_enh = list(zip(*plot_data_enh))
    # np.save('degraded', plot_data_deg)
    # np.save('arcnn', plot_data_enh)
    # plt.plot(ticks[:len(f1_vals_enh)], f1_vals_enh, 'g')
    # plt.plot(ticks[:len(f1_vals_deg)], f1_vals_deg, 'b')
    plt.plot(plot_data_enh[0], plot_data_enh[1], 'g', linewidth=0.5)
    plt.plot(plot_data_deg[0], plot_data_deg[1], 'b', linewidth=0.5)
    plt.grid(color='k', linestyle='-', linewidth=0.25)
    plt.legend(('Enhanced', 'Degraded'))
    plt.xlabel('FPR, AUC: D: %0.3f, E: %0.3f' % (auc_deg, auc_enh))
    plt.ylabel('TPR')
    plt.show()
    plt.savefig('ROC_Graph', dpi=None, facecolor='w', edgecolor='w',
                orientation='portrait', papertype=None, format=None,
                transparent=False, bbox_inches=None, pad_inches=0.1,
                frameon=None)


# our = np.load('our.npy')
# degraded = np.load('degraded.npy')
# iegan = np.load('iegan.npy')
# ircnn = np.load('ircnn.npy')
# arcnn = np.load('arcnn.npy')
#
# plt.plot(our[0], our[1], 'g', linewidth=0.5)
# plt.plot(degraded[0], degraded[1], 'b', linewidth=0.5)
# plt.plot(iegan[0], iegan[1], 'r', linewidth=0.5)
# plt.plot(ircnn[0], ircnn[1], 'k', linewidth=0.5)
# plt.plot(arcnn[0], arcnn[1], 'c', linewidth=0.5)
# plt.grid(color='k', linestyle='-', linewidth=0.25)
# plt.legend(('OURS', 'Original', 'IEGAN', 'IRCNN', 'ARCNN'))
# plt.xlabel('FPR')#, AUC: D: %0.3f, E: %0.3f' % (auc_deg, auc_enh))
# plt.ylabel('TPR')
# plt.show()
# plt.savefig('ROC_Graph', dpi=None, facecolor='w', edgecolor='w',
#             orientation='portrait', papertype=None, format=None,
#             transparent=False, bbox_inches=None, pad_inches=0.1,
#             frameon=None)