from random import randint
import logging.config
import glob
import random
import os
from PIL import Image
import sys
import tensorflow as tf
import logging
import logging.config
import numpy as np
import errno
import cv2
from tqdm import tqdm

from configparser import ConfigParser

parser = ConfigParser()

try:
    logging.config.fileConfig('../logging.ini')
    parser.read('../config.ini')
except Exception as e:
    logging.config.fileConfig('logging.ini')
    parser.read('config.ini')
IMG_FILE = ['jpg', 'png']


# num_folders = int(parser.get('mongoDB', 'num_folders'))

def save(sess, filepath='../tmp/tfmodel.mdl', global_step=None):
    """
    Save a TensorFlow model.
    :param sess:
    :param filepath:
    :param global_step:
    :return:
    """
    saver = tf.train.Saver()
    saver.save(sess, filepath, global_step=global_step)
    logging.info('Model saved at ' + filepath)


def load(sess, filepath, var=None):
    """
    Load/Restore a TensorFlow model.
    :param sess: The session
    :param filepath:
    :return:
    """
    init_op = tf.global_variables_initializer()
    sess.run(init_op)
    if var is None:
        saver = tf.train.Saver()
    else:
        saver = tf.train.Saver(var)
    saver.restore(sess, filepath)
    logging.info('Model restored from ' + filepath)
    return sess


def save_image(image, filename, ext='.jpg'):
    """
    Save a numpy array to image.
    :param image: The numpy array
    :param filename:
    :param ext:
    :return:
    """
    try:
        os.makedirs(os.path.dirname(filename))
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(os.path.dirname(filename)):
            pass
        else:
            raise
    cv2.imwrite(filename + ext, image)


def average_gradients(tower_grads):
    """Calculate the average gradient for each shared variable across all towers.
    Note that this function provides a synchronization point across all towers.
    Args:
      tower_grads: List of lists of (gradient, variable) tuples. The outer list
        is over individual gradients. The inner list is over the gradient
        calculation for each tower.
    Returns:
       List of pairs of (gradient, variable) where the gradient has been averaged
       across all towers.
    """
    average_grads = []
    for grad_and_vars in zip(*tower_grads):
        # Note that each grad_and_vars looks like the following:
        #   ((grad0_gpu0, var0_gpu0), ... , (grad0_gpuN, var0_gpuN))
        grads = []
        for g, _ in grad_and_vars:
            # Add 0 dimension to the gradients to represent the tower.
            expanded_g = tf.expand_dims(g, 0)

            # Append on a 'tower' dimension which we will average over below.
            grads.append(expanded_g)

        # Average over the 'tower' dimension.
        grad = tf.concat(axis=0, values=grads)
        grad = tf.reduce_mean(grad, 0)

        # Keep in mind that the Variables are redundant because they are shared
        # across towers. So .. we will just return the first tower's pointer to
        # the Variable.
        v = grad_and_vars[0][1]
        grad_and_var = (grad, v)
        average_grads.append(grad_and_var)
    return average_grads


def rename_files(folder, level=1):
    if level == 0:
        count = 0
        for filename in glob.glob(folder + '/*.jpg'):
            newname = os.path.basename(folder) + '_' + str(count) + '.jpg'
            os.rename(src=filename, dst=os.path.join(folder, newname))
            count += 1
    elif level == 1:
        for foldername in glob.iglob(folder + '/*/'):
            count = 0
            foldername_2 = foldername.replace('DEGRADED', '')
            for filename in glob.glob(foldername + '/*.jpg'):
                newname = os.path.basename(foldername[:-1]) + '_' + '%03d' % count + '.jpg'
                os.rename(filename, os.path.join(foldername, newname))
                filename_2 = filename.replace('DEGRADED', '')
                os.rename(filename_2, os.path.join(foldername_2, newname))
                count += 1


class DataGenerator(object):
    def __init__(self, data_path, gt_path=None, ignore_list_path=None, **kwargs):
        """
        Generator to yield data in proper format. The tree structure of the image organization is in the documentation.
        :param gt_path:
        :param data_path:
        :param ignore_list_path:
        :param data_resolution: The resolution of the training/testing data to be yielded.
        :param gt_resolution: The resolution of the ground truth data to be yielded.
        :param front_pose_data_resolution: Training data and ground truth resolution for frontal pose generation data
                                            are the same, so this parameter holds that resolution.
        """
        allowed_kwargs = {'data_resolution',
                          'gt_resolution',
                          'front_pose_data_resolution'}
        for k in kwargs:
            if k not in allowed_kwargs:
                raise TypeError('Unexpected keyword argument passed: ' + str(k))
        self.gt_path = gt_path
        self.data_path = data_path

        self.ignore_list_path = ignore_list_path
        self.data_resolution = (64, 64)
        self.gt_resolution = (64, 64)
        self.__dict__.update(kwargs)

    def yield_predict_batch_ar(self, batch_size):
        """

        :param batch_size:
        :return:
        """
        for img_path in glob.iglob(self.data_path + '/*.jpg'):
            if os.path.exists(img_path):
                image = Image.open(img_path)
                w, h = image.size
                if w > 256 and h > 256:
                    data_batch = np.zeros(shape=(batch_size,) + (h, w, 1))
                    data_batch[0, :, :, :] = np.reshape(image, (h, w, 1))
                    yield data_batch / 255., img_path.replace(self.data_path + '/', '')

    def yield_ar_batch(self, batch_size):
        """
        This method yields the training data and the labels for artifact removal.
        :param batch_size:
        :return:
        """
        batch_counter = 0
        if self.ignore_list_path is not None:
            ignore_folders = self.no_gt_folders()
        else:
            ignore_folders = []
        res = self.gt_resolution
        data_batch = np.zeros(shape=(batch_size,) + res + (3,))
        ground_truth_batch = np.zeros(shape=(batch_size,) + res + (3,))
        dirs = ['/%s' % i for i in os.listdir(self.data_path) if
                os.path.isdir('%s/%s' % (self.data_path, i)) and i not in ignore_folders]
        for i in range(200):
            for img_dir in dirs:
                img_path = self.data_path + img_dir + '/' + str(i) + '.jpg'
                label_path = self.gt_path + img_dir + '/' + str(i) + '.jpg'
                if os.path.exists(img_path):
                    # image = Image.open(img_path)
                    image = Image.open(img_path).resize(self.gt_resolution, Image.ANTIALIAS)
                    try:
                        ground_truth = Image.open(label_path).resize(self.gt_resolution, Image.ANTIALIAS)
                    except IOError:
                        logging.error('Ground truth not found in %s' % label_path)
                        raise
                    if batch_counter < batch_size:
                        data_batch[batch_counter, :, :, :] = image
                        ground_truth_batch[batch_counter, :, :, :] = ground_truth
                        batch_counter += 1
                    else:
                        batch_counter = 0
                        yield data_batch / 255., ground_truth_batch / 255.

    def yield_ar_batch_v2(self, batch_size):
        """
        A method specially built for yielding AR data batches for the Luma (Y of YCbCr) channel only.
        :param batch_size:
        :return:
        """
        batch_counter = 0
        data_batch = np.zeros(shape=(batch_size,) + self.data_resolution + (1,))
        gt_batch = np.zeros(shape=(batch_size,) + self.gt_resolution + (1,))
        dx = dy = 256  # resolution of each image
        for img_path in glob.iglob(self.data_path + '/*.JPEG'):
            gt_path = img_path.replace('_LR_10', '')  # hardcoded for our imagenet subset
            gt_path = gt_path.replace('_LR_20', '')  # hardcoded for our imagenet subset
            if os.path.exists(gt_path):
                img = Image.open(gt_path)
                # data augmentation by random cropping
                w, h = img.size
                if w > 256 and h > 256:
                    x = random.randint(0, w - dx - 1)
                    y = random.randint(0, h - dy - 1)
                    img = img.crop((x, y, x + dx, y + dy))
                    # image_hr = np.array(img.convert('YCbCr'))[:, :, 0]  # Take only the Y channel
                    image_hr = img.convert('YCbCr').split()[0]  # Take only the Y channel
                    image_lr = Image.open(img_path).crop((x, y, x + dx, y + dy))
                    if batch_counter < batch_size:
                        data_batch[batch_counter, :, :, :] = np.reshape(image_lr, (256, 256, 1))
                        gt_batch[batch_counter, :, :, :] = np.reshape(image_hr, (256, 256, 1))
                        batch_counter += 1
                    else:
                        batch_counter = 0
                        yield data_batch / 255., gt_batch / 255.

    def yield_data_renren_ar_RGB(self, batch_size, start_folder, end_folder, train=True, channel=3):
        """

        :param batch_size: Batch size
        :param start_folder: Start folder number for test set
        :param end_folder: End folder number for test set
        :param train: If True, it ignores the folders from start to end, else it returns from those folders
        :param channel: 3 for colour images and 1 for Luma channel only (B&W)
        :return:
        """
        batch_counter = 0
        assert self.data_resolution == self.gt_resolution
        data_batch = np.zeros(shape=(batch_size,) + self.data_resolution + (channel,))
        gt_batch = np.zeros(shape=(batch_size,) + self.gt_resolution + (channel,))
        filenames = []
        for tr_img_dir in glob.iglob(self.data_path + '*/'):
            folder_num = tr_img_dir[-6:-1]
            cond_1 = int(start_folder) <= int(folder_num) <= int(end_folder) and train
            cond_2 = not (int(start_folder) <= int(folder_num) <= int(end_folder)) and not train
            logging.debug(str(cond_1) + '----' + str(cond_2))
            if cond_1 or cond_2:
                pass
            else:
                if int(start_folder) <= int(folder_num) <= int(end_folder):
                    logging.debug('Generating test batch')
                for tr_img_path in glob.iglob(tr_img_dir + '*.jpg'):
                    gt_img_path = tr_img_path.replace('DEGRADED', '')
                    if os.path.exists(gt_img_path):
                        # image_tr = Image.open(tr_img_path).resize(self.data_resolution)
                        # image_gt = Image.open(gt_img_path).resize(self.gt_resolution)
                        ## If working only on the Luma Channel
                        # image_tr = image_tr.convert('YCbCr').split()[0]  # Take only the Y channel
                        # image_gt = image_gt.convert('YCbCr').split()[0]  # Take only the Y channel
                        image_tr = Image.open(tr_img_path).resize(self.data_resolution)
                        image_gt = Image.open(gt_img_path).resize(self.gt_resolution)
                        data_batch[batch_counter, :, :, :] = np.reshape(image_tr, (64, 64, channel))
                        gt_batch[batch_counter, :, :, :] = np.reshape(image_gt, (64, 64, channel))
                        filenames.append(
                            os.path.join(os.path.basename(tr_img_dir[:-1]), os.path.basename(gt_img_path)))
                        batch_counter += 1
                        if batch_counter == batch_size:
                            batch_counter = 0
                            if not train:
                                logging.debug('Unable to yield test images')
                            yield data_batch / 255., gt_batch / 255., filenames
                            filenames = []
                    else:
                        raise IOError('%s does not exist' % gt_img_path)

    def yield_data_renren_ar_BGR(self, batch_size, start_folder, end_folder, train=True, channel=3):
        """

        :param batch_size: Batch size
        :param start_folder: Start folder number for test set
        :param end_folder: End folder number for test set
        :param train: If True, it ignores the folders from start to end, else it returns from those folders
        :param channel: 3 for colour images and 1 for Luma channel only (B&W)
        :return:
        """
        batch_counter = 0
        self.data_resolution_, self.gt_resolution_ = (128, 128), (128, 128)
        assert self.data_resolution_ == self.gt_resolution_
        data_batch = np.zeros(shape=(batch_size,) + self.data_resolution_ + (channel,))
        gt_batch = np.zeros(shape=(batch_size,) + self.gt_resolution_ + (channel,))
        filenames = []
        each_side = self.data_resolution_[0]
        for tr_img_dir in glob.iglob(self.data_path + '*/'):
            folder_num = tr_img_dir[-6:-1]
            cond_1 = int(start_folder) <= int(folder_num) <= int(end_folder) and train
            cond_2 = not (int(start_folder) <= int(folder_num) <= int(end_folder)) and not train
            logging.debug(str(cond_1) + '----' + str(cond_2))
            if cond_1 or cond_2:
                pass
            else:
                if int(start_folder) <= int(folder_num) <= int(end_folder):
                    logging.debug('Generating test batch')
                for tr_img_path in glob.iglob(tr_img_dir + '*.jpg'):
                    gt_img_path = tr_img_path.replace('DEGRADED_mixed', '')
                    if os.path.exists(gt_img_path):
                        # image_tr = cv2.resize(cv2.imread(tr_img_path), self.data_resolution)
                        # image_gt = cv2.resize(cv2.imread(gt_img_path), self.gt_resolution)
                        image_tr = cv2.resize(cv2.imread(tr_img_path), self.data_resolution_)
                        image_gt = cv2.resize(cv2.imread(gt_img_path), self.gt_resolution_)
                        data_batch[batch_counter, :, :, :] = np.reshape(image_tr, (each_side, each_side, channel))
                        gt_batch[batch_counter, :, :, :] = np.reshape(image_gt, (each_side, each_side, channel))
                        filenames.append(
                            os.path.join(os.path.basename(tr_img_dir[:-1]), os.path.basename(gt_img_path)))
                        batch_counter += 1
                        if batch_counter == batch_size:
                            batch_counter = 0
                            if not train:
                                logging.debug('Unable to yield test images')
                            yield data_batch / 255., gt_batch / 255., filenames
                            filenames = []
                    else:
                        continue
            # raise IOError('%s does not exist' % gt_img_path)

    def yield_data_renren_shuffled_BGR(self, batch_size, start_folder, end_folder, train=True, channel=3):
        """

            :param batch_size: Batch size
            :param start_folder: Start folder number for test set
            :param end_folder: End folder number for test set
            :param train: If True, it ignores the folders from start to end, else it returns from those folders
            :param channel: 3 for colour images and 1 for Luma channel only (B&W)
            :return:
        """

        batch_counter = 0
        assert self.data_resolution == self.gt_resolution
        data_batch = np.zeros(shape=(batch_size,) + self.data_resolution + (channel,))
        gt_batch = np.zeros(shape=(batch_size,) + self.gt_resolution + (channel,))
        filenames = []
        each_side = self.data_resolution[0]
        folders = 72412
        for img_num in range(1, 201, 1):
            if train:
                for folder_num in range(int(end_folder) + 1, folders, 1):
                    img_name = '%05d_%03d.jpg' % (folder_num, img_num)
                    tr_img_path = os.path.join(self.data_path + '%05d' % folder_num, img_name)
                    gt_img_path = tr_img_path.replace('DEGRADED', '')
                    if not os.path.exists(gt_img_path):
                        gt_img_path = tr_img_path.replace('DEGRADED_mixed', '')
                    if os.path.exists(gt_img_path) and os.path.exists(tr_img_path):
                        image_tr = cv2.resize(cv2.imread(tr_img_path), self.data_resolution)
                        image_gt = cv2.resize(cv2.imread(gt_img_path), self.gt_resolution)
                        data_batch[batch_counter, :, :, :] = np.reshape(image_tr, (each_side, each_side, channel))
                        gt_batch[batch_counter, :, :, :] = np.reshape(image_gt, (each_side, each_side, channel))
                        filenames.append(img_name)
                        batch_counter += 1
                        if batch_counter == batch_size:
                            batch_counter = 0
                            if not train:
                                logging.debug('Unable to yield test images')
                            yield data_batch / 255., gt_batch / 255., filenames
                            filenames = []
                    else:
                        continue
            else:
                for folder_num in range(int(start_folder) + 1, int(end_folder), 1):
                    img_name = '%05d_%03d.jpg' % (folder_num, img_num)
                    tr_img_path = os.path.join(self.data_path + '%05d' % folder_num, img_name)
                    gt_img_path = tr_img_path.replace('DEGRADED', '')
                    if not os.path.exists(gt_img_path):
                        gt_img_path = tr_img_path.replace('DEGRADED_mixed', '')
                    if os.path.exists(gt_img_path) and os.path.exists(tr_img_path):
                        image_tr = cv2.resize(cv2.imread(tr_img_path), self.data_resolution)
                        image_gt = cv2.resize(cv2.imread(gt_img_path), self.gt_resolution)
                        data_batch[batch_counter, :, :, :] = np.reshape(image_tr, (each_side, each_side, channel))
                        gt_batch[batch_counter, :, :, :] = np.reshape(image_gt, (each_side, each_side, channel))
                        filenames.append(img_name)
                        batch_counter += 1
                        if batch_counter == batch_size:
                            batch_counter = 0
                            if not train:
                                logging.debug('Unable to yield test images')
                            yield data_batch / 255., gt_batch / 255., filenames
                            filenames = []
                    else:
                        continue

    def yield_data_renren_ar_fast(self, batch_size, start_folder, end_folder, train=True, channel=3):
        """
        This is a faster version of yield_data_renren_ar_RGB but with some memory overhead.
        :param batch_size: Batch size
        :param start_folder: Start folder number for test set
        :param end_folder: End folder number for test set
        :param train: If True, it ignores the folders from start to end, else it returns from those folders
        :param channel: 3 for colour images and 1 for Luma channel only (B&W)
        :return:
        """
        batch_counter = 0
        assert self.data_resolution == self.gt_resolution
        data_batch = np.zeros(shape=(batch_size,) + self.data_resolution + (channel,))
        gt_batch = np.zeros(shape=(batch_size,) + self.gt_resolution + (channel,))
        filenames = []

        def is_between(dir_):
            folder_num = int(os.path.basename(os.path.normpath(dir_)))
            if int(start_folder) <= int(folder_num) <= int(end_folder):
                return True
            else:
                return False

        tr_img_dir_list = []
        test_img_dir_list = []
        for dir_ in glob.glob(self.data_path + '*/'):
            if is_between(dir_):
                test_img_dir_list.append(dir_)
            else:
                tr_img_dir_list.append(dir_)
        if train:
            img_dir_list = tr_img_dir_list
        else:
            img_dir_list = test_img_dir_list
        for tr_img_dir in img_dir_list:
            for tr_img_path in glob.glob(tr_img_dir + '*.jpg'):
                gt_img_path = tr_img_path.replace('DEGRADED', '')
                if os.path.exists(gt_img_path):
                    image_tr = Image.open(tr_img_path).resize(self.data_resolution)
                    image_gt = Image.open(gt_img_path).resize(self.gt_resolution)
                    ## If working only on the Luma Channel
                    # image_tr = image_tr.convert('YCbCr').split()[0]  # Take only the Y channel
                    # image_gt = image_gt.convert('YCbCr').split()[0]  # Take only the Y channel
                    if batch_counter < batch_size:
                        data_batch[batch_counter, :, :, :] = np.reshape(image_tr, (64, 64, channel))
                        gt_batch[batch_counter, :, :, :] = np.reshape(image_gt, (64, 64, channel))
                        filenames.append(os.path.basename(gt_img_path))
                        batch_counter += 1
                    else:
                        batch_counter = 0
                        if not train:
                            logging.debug('Unable to yield test images')
                        yield data_batch / 255., gt_batch / 255., filenames
                else:
                    raise IOError('%s does not exist' % gt_img_path)

    def yield_data_for_score(self, start_folder, end_folder, train=False):
        """

            :param batch_size: Batch size
            :param start_folder: Start folder number for test set
            :param end_folder: End folder number for test set
            :param train: If True, it ignores the folders from start to end, else it returns from those folders
            :param channel: 3 for colour images and 1 for Luma channel only (B&W)
            :return:
        """
        for ori_img_dir in glob.iglob(self.data_path + '/*/'):
            folder_num = ori_img_dir[-6:-1]
            cond_1 = int(start_folder) <= int(folder_num) <= int(end_folder) and train
            cond_2 = not (int(start_folder) <= int(folder_num) <= int(end_folder)) and not train
            logging.debug(str(cond_1) + '----' + str(cond_2))
            if cond_1 or cond_2:
                pass
            else:
                if int(start_folder) <= int(folder_num) <= int(end_folder):
                    logging.debug('Generating test batch')
                for ori_img_path in glob.iglob(ori_img_dir + '*.jpg'):
                    deg_img_path = ori_img_path.replace('renren_', 'renren_DEGRADED')
                    enh_img_path = ori_img_path.replace('renren_', 'renren_ENHANCED')
                    gt_img_path = ori_img_dir.replace('renren_', 'renren_GT') + 'gt.jpg'
                    filename = os.path.join(os.path.basename(ori_img_dir[:-1]), os.path.basename(ori_img_path))
                    exist_all = os.path.exists(deg_img_path) and os.path.exists(enh_img_path)
                    if os.path.exists(gt_img_path) and exist_all:
                        try:
                            image_ori = cv2.resize(cv2.imread(ori_img_path), (128, 128))
                            image_deg = cv2.resize(cv2.imread(deg_img_path), (128, 128))
                            image_enh = cv2.resize(cv2.imread(enh_img_path), (128, 128))
                            # image_gt = Image.open(gt_img_path)
                            yield image_ori, image_deg, image_enh, filename
                        except Exception as e:
                            logging.error(str(e))
                            logging.info(filename + 'is corrupted')
                            yield None, None, None, filename
                        ## If working only on the Luma Channel
                        # image_tr = image_tr.convert('YCbCr').split()[0]  # Take only the Y channel
                        # image_gt = image_gt.convert('YCbCr').split()[0]  # Take only the Y channel
                    else:
                        logging.warning('GT doesn\'t exist for ' + gt_img_path)
                        yield None, None, None, filename

    def yield_data_for_score_no_original(self, deg_path, enh_path, extension='png'):
        """

            :param batch_size: Batch size
            :param start_folder: Start folder number for test set
            :param end_folder: End folder number for test set
            :param train: If True, it ignores the folders from start to end, else it returns from those folders
            :param channel: 3 for colour images and 1 for Luma channel only (B&W)
            :return:
        """

        for ori_img_dir in glob.iglob(self.data_path + '/*/'):
            logging.debug('Generating test batch')
            for ori_img_path in glob.iglob(ori_img_dir + '*.' + extension):
                image_fname = os.path.join(os.path.basename(os.path.dirname(ori_img_path)),
                                           os.path.basename(ori_img_path))
                deg_img_path = os.path.join(deg_path, image_fname)
                enh_img_path = os.path.join(enh_path, image_fname)
                if os.path.exists(deg_img_path) and os.path.exists(enh_img_path):
                    try:
                        image_deg = cv2.resize(cv2.imread(deg_img_path), (128, 128))
                        image_enh = cv2.resize(cv2.imread(enh_img_path), (128, 128))
                        # image_gt = Image.open(gt_img_path)
                        yield image_deg, image_enh, image_fname
                    except Exception as e:
                        logging.error(str(e))
                        logging.info(image_fname + 'is corrupted')
                        yield None, None, image_fname
                else:
                    logging.warning('GT doesn\'t exist for ' + image_fname)
                    yield None, None, image_fname

    def yield_data_test_any(self, batch_size, extension='png'):
        """

        :param batch_size: Batch size
        :param start_folder: Start folder number for test set
        :param end_folder: End folder number for test set
        :param train: If True, it ignores the folders from start to end, else it returns from those folders
        :param channel: 3 for colour images and 1 for Luma channel only (B&W)
        :return:
        """
        batch_counter = 0
        filenames = []
        data_batch = []
        for tr_img_dir in glob.iglob(self.data_path + '*/'):
            image_generator = glob.glob(tr_img_dir + '*.' + extension)
            for tr_img_path in image_generator:
                image_tr = cv2.resize(cv2.imread(tr_img_path), (128, 128))
                # image_tr = cv2.imread(tr_img_path)
                # data_batch = np.expand_dims(image_tr, 0)
                data_batch.append(np.expand_dims(image_tr, 0))
                filenames.append(os.path.join(os.path.basename(tr_img_dir[:-1]), os.path.basename(tr_img_path)))
                batch_counter += 1
                if batch_counter == batch_size:
                    batch_counter = 0
                    data_batch = np.concatenate(data_batch, axis=0)
                    yield data_batch / 255., filenames
                    filenames = []
                    data_batch = []
            # t.update(n=1)
        # t.close()


if __name__ == '__main__':
    # gt_path_ = parser.get('DataGeneratorAR', 'gt')
    # tr_path_ = parser.get('DataGeneratorAR', 'tr')
    # start_folder = parser.get('DataGeneratorAR', 'test_data_start_folder')
    # end_folder = parser.get('DataGeneratorAR', 'test_data_end_folder')
    # batch_size_ = int(parser.get('main', 'batch_size'))
    # gen = DataGenerator(tr_path_, gt_path_)
    # for i, j in gen.yield_data_renren_ar_RGB(batch_size_, start_folder, end_folder, train=False):
    #     pass
    rename_files('/work/ms_anyv_umd_MiddleAsian_renren_DEGRADED')
