# Take a single image and get the faces and features
import cv2
import numpy as np
import sys
# sys.path.append('/home/soumya/Downloads/caffe/python')
# import caffe
import tensorflow as tf
from PIL import Image


def getFeatures(sess, netw_op, image, img_placeholder):
    if sess is None:
        image = np.squeeze(image)
        image = np.uint8(image * 255)
        try:
            features = np.expand_dims(netw_op(image)[0], axis=0)
        except IndexError:
            features = np.zeros(shape=(1, 128))
    else:
        features = sess.run(fetches=netw_op, feed_dict={img_placeholder: image})
    return features


def getFeatures_fast(net, image):
    image = np.array(image.resize((128, 128)))
    # image = cv2.resize(image, (128, 128))
    face_batch_data = np.zeros((1, 3, 128, 128))
    if np.amax(image) > 1:
        image = image / 255.0
    face_batch_data[0, 0, :, :] = image[:, :, 0]
    face_batch_data[0, 1, :, :] = image[:, :, 1]
    face_batch_data[0, 2, :, :] = image[:, :, 2]
    net.blobs['data'].reshape(*face_batch_data.shape)
    out = net.forward_all(data=face_batch_data)
    feature = np.float32(out["fc1"])
    res = np.reshape(feature, (face_batch_data.shape[0], 256))
    return res


if __name__ == "__main__":
    # image_1 = cv2.imread("101.jpg")
    image_1 = Image.open("101.jpg")
    # image_2 = cv2.imread("101.jpg")
    image_2 = Image.open("101.jpg")

    net = caffe.Net("caffe_model/v6_bgr.prototxt", "caffe_model/v6_bgr.caffemodel", caffe.TEST)

    features = np.dot(getFeatures_fast(net, image_2), getFeatures(net, image_1).T) / (
                np.linalg.norm(getFeatures(net, image_1)) * np.linalg.norm(getFeatures_fast(net, image_2)))
    # norm = np.linalg.norm(v.meanFeatures)
    print("Extracted features, vector size={}".format(features.shape))

    # cv2.imshow("face", image_1)
    cv2.waitKey(100)
    print
    features
