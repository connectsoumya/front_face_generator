from __future__ import print_function
import tensorflow as tf
from tensorflow import errors
import os
from core.model import Discriminator
from core.model import FARNet
from core.metrics import cosine_distance
from utils.data_pipeline import data_generator_renren
from utils.misc import Params
from utils.ops import average_gradients
from utils.ops import save
from utils.ops import load
from other_algos.vgg19api import vgg19
from other_algos.anv_v6_tf_api import anvv6
from core.metrics import mse_loss, canny_loss
from evaluate._utils import DataGenerator
from evaluate._utils import save_image
from core.metrics import lossless_triplet_loss
import tensorlayer as tl
import numpy as np
import copy
import logging.config
import logging
from configparser import ConfigParser

# GENERAL CONFIG
parser = ConfigParser()
parser.read('config.ini')
logging.config.fileConfig('logging.ini')
# CUDA CONFIG
os.environ["CUDA_VISIBLE_DEVICES"] = parser.get('GPU', 'cuda_visible_device')
config = tf.ConfigProto(allow_soft_placement=True)
config.gpu_options.allow_growth = True

param = Params()
param.epochs = int(parser.get('PARAMS', 'epochs'))
param.batch_size = int(parser.get('PARAMS', 'batch_size'))
param.num_classes = int(parser.get('PARAMS', 'num_classes'))
param.lr = float(parser.get('PARAMS', 'lr'))
param.num_parallel_calls = int(parser.get('PARAMS', 'threads'))
param.gpu = parser.get('GPU', 'device').split()

model_save_path = parser.get('IO', 'model_save_path')
model_load_path = parser.get('IO', 'model_load_path')
txt_file = parser.get('DATA', 'renren_names')
renren_label = parser.get('DATA', 'renren_labels')
renren_degraded = parser.get('DATA', 'renren_degraded')
test_data_degraded = parser.get('TESTING', 'test_data_path')
test_data_enhanced = parser.get('TESTING', 'enhanced_data_path')


class FaceHallucinate(object):
    def __init__(self, params, alpha=20, beta=250, gamma_c=1, gamma_p=10e-3):
        self.params = params
        self.alpha = alpha
        self.beta = beta
        self.gamma_c = gamma_c
        self.gamma_p = gamma_p
        self.num_gpu = len(params.gpu)
        assert np.mod(self.params.batch_size, self.num_gpu) == 0
        self.optimizer = tf.train.AdamOptimizer(learning_rate=self.params.lr)

    def _tensorboard(self, sess):
        tf.summary.scalar('loss/adv_loss', self.adv_loss)
        tf.summary.scalar('loss/per_loss', self.perceptual_loss)
        tf.summary.scalar('loss/gen_loss', self.gen_loss)
        tf.summary.scalar('loss/id_loss_end', self.id_loss_end)
        tf.summary.scalar('loss/id_loss_mid', self.id_loss_mid)
        tf.summary.scalar('loss/triplet_loss_mid', self.triplet_loss_mid)
        tf.summary.scalar('loss/triplet_loss_end', self.triplet_loss_end)
        tf.summary.scalar('loss/tot_loss', self.total_loss)
        self.summary_op = tf.summary.merge_all()
        self.train_writer = tf.summary.FileWriter('data/tensorboard', sess.graph)

    def train(self):
        self.degraded_image = tf.placeholder(dtype=tf.float32, shape=(None, 128, 128, 3))
        self.labels = tf.placeholder(dtype=tf.float32, shape=(None, 128, 128, 3))
        self.original_image = tf.placeholder(dtype=tf.float32, shape=(None, 128, 128, 3))
        self.anchor = tf.placeholder(dtype=tf.float32, shape=(None, 256), name='anchor')
        self.neg_feat = tf.placeholder(dtype=tf.float32, shape=(None, 256), name='neg_feat')
        # Generator
        g = FARNet(self.params.batch_size, reuse=tf.AUTO_REUSE)
        # Discriminator
        d = Discriminator(self.params.batch_size)
        # list for tower data
        tower_grads = []
        tower_grads_2 = []
        tower_image_degraded = tf.split(self.degraded_image, self.num_gpu)
        tower_labels = tf.split(self.labels, self.num_gpu)
        tower_image_original = tf.split(self.original_image, self.num_gpu)
        tower_anc = tf.split(self.anchor, self.num_gpu)
        tower_neg_feat = tf.split(self.neg_feat, self.num_gpu)
        gpu_id = 0
        for gpu_num in self.params.gpu:
            with tf.device(gpu_num):
                # Network G
                with tf.variable_scope('net_g', reuse=tf.AUTO_REUSE):
                    image_coarse = g.coarse_ar_net(tower_image_degraded[gpu_id], reuse=tf.AUTO_REUSE)
                    image_enc = g.fine_ar_enc(image_coarse, reuse=tf.AUTO_REUSE)
                    image_pmaps = g.prior_est_net(image_coarse, reuse=tf.AUTO_REUSE)
                    image_sum = image_enc + image_pmaps
                    output = g.fine_ar_dec(image_sum, reuse=tf.AUTO_REUSE)
                # Network D
                dis_ip = tf.concat([output, tower_image_degraded[gpu_id]], axis=0)
                with tf.variable_scope('net_d'):
                    d_fake_logit = d.dcgan(output, reuse=tf.AUTO_REUSE)
                    d_real_logit = d.dcgan(tower_image_degraded[gpu_id], reuse=True)
                    d_logit = d.dcgan(dis_ip, reuse=tf.AUTO_REUSE)
                d_fake_label = tf.zeros_like(d_fake_logit)
                d_real_label = tf.ones_like(d_real_logit)
                d_label = tf.concat([d_fake_label, d_real_label], axis=0)
                d_loss = tf.nn.sigmoid_cross_entropy_with_logits(labels=d_label, logits=d_logit)
                self.adv_loss = tf.log(tf.reduce_mean(d_loss))
                # Losses
                self.gen_loss = tf.reduce_sum(tf.norm(tower_labels[gpu_id] - image_coarse) + tf.norm(
                    tower_labels[gpu_id] - output) + self.beta * tf.norm(
                    tower_image_original[gpu_id] - image_pmaps)) / (2 * self.params.batch_size)
                self.net_vgg = vgg19(tower_image_original[gpu_id], tf.AUTO_REUSE)
                self.net_anvv6 = anvv6(tower_image_original[gpu_id], tf.AUTO_REUSE)
                mid_feature = anvv6(image_coarse, True).outputs
                end_feature = anvv6(output, True).outputs
                self.id_loss_mid = cosine_distance(self.net_anvv6.outputs, mid_feature)
                self.id_loss_end = cosine_distance(self.net_anvv6.outputs, end_feature)
                self.perceptual_loss = tf.norm(
                    tf.reduce_mean(self.net_vgg.outputs - vgg19(output, True).outputs, axis=0))
                self.triplet_loss_mid = lossless_triplet_loss(tower_anc[gpu_id], mid_feature, tower_neg_feat[gpu_id])
                self.triplet_loss_end = lossless_triplet_loss(tower_anc[gpu_id], end_feature, tower_neg_feat[gpu_id])
                self.total_loss = self.gen_loss + (self.gamma_p * self.perceptual_loss) - (
                        self.gamma_c * self.adv_loss) + (self.alpha * self.id_loss_end) + (
                                          self.beta * self.triplet_loss_end) + (
                                          self.alpha * self.id_loss_mid) + (
                                          self.beta * self.triplet_loss_mid) + (self.beta * self.triplet_loss_end)
                # Get the trainable variables of the generator and discriminator
                trainable_vars = tf.trainable_variables()
                net_g_vars = [var for var in trainable_vars if 'net_g' in var.name]
                net_d_vars = [var for var in trainable_vars if 'net_d' in var.name]
                tower_grads.append(
                    self.optimizer.compute_gradients(loss=self.total_loss, var_list=net_g_vars))
                tower_grads_2.append(
                    self.optimizer.compute_gradients(loss=self.total_loss, var_list=net_d_vars))
            gpu_id += 1
        # Get the average of all the tower gradients
        grad = average_gradients(tower_grads)
        grad_2 = average_gradients(tower_grads_2)
        # Apply the gradients to the model
        self.opt = self.optimizer.apply_gradients(grad)
        self.opt_2 = self.optimizer.apply_gradients(grad_2)
        anchors = np.load('data/anchors.npy')
        # Other required computations
        with open(txt_file) as f:
            degraded_foldernames = [os.path.join(renren_degraded, file.strip('\n')) for file in f.readlines()]
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            self._tensorboard(sess)
            data = data_generator_renren(degraded_foldernames, param)
            sess.run(data.initializer)
            # ===== LOAD MODEL =====
            try:
                load(sess, os.path.join(model_load_path, 'weights.mdl'))
                print('Previous model loaded from %s' % model_load_path)
            except errors.InvalidArgumentError as e:
                logging.info(str(e) + 'No models found. Training from scratch...\n')
            except ValueError as e:
                logging.info(str(e) + 'No models found. Training from scratch...\n')
            except errors.NotFoundError as e:
                logging.info('Model of different architecture found. Aborting...\n' + str(e))
            # ===== LOAD VGG =====
            if not os.path.isfile('data/vgg19.npy'):
                print('Please download vgg19.npz from : https://github.com/machrisaa/tensorflow-vgg')
                exit()
            npz = np.load('data/vgg19.npy', encoding='latin1').item()
            params = []
            for val in sorted(npz.items()):
                W = np.asarray(val[1][0])
                b = np.asarray(val[1][1])
                print("  Loading %s: %s, %s" % (val[0], W.shape, b.shape))
                params.extend([W, b])
            tl.files.assign_params(sess, params[:30], self.net_vgg)
            # # ===== LOAD ANV_V6 =====
            params = np.load('data/anv_v6.weights.npy', encoding='latin1')
            tl.files.assign_params(sess, params, self.net_anvv6)
            # =======================
            self.iterations = 0
            next_batch = data.get_next()

            for ep in range(self.params.epochs):
                while True:
                    try:
                        degraded_images, original_images, gt_images, fnamex = sess.run(next_batch)
                    except errors.OutOfRangeError:
                        sess.run(data.initializer)
                        break
                    except errors.InvalidArgumentError:
                        sess.run(data.initializer)
                        break
                    except errors.NotFoundError:
                        continue
                    anchors_pos_idx = [int(a.split('/')[-2]) for a in fnamex]
                    anchors_neg_idx = copy.deepcopy(anchors_pos_idx)
                    anchors_neg_idx.insert(0, anchors_neg_idx.pop(-1))
                    anchors_pos = anchors[anchors_pos_idx]
                    anchors_neg = anchors[anchors_neg_idx]
                    feed_dict_ = {self.degraded_image: degraded_images,
                                  self.original_image: original_images,
                                  self.labels: gt_images,
                                  self.neg_feat: anchors_neg,
                                  self.anchor: anchors_pos}
                    _, totloss, advloss, lo_per, lo_gen, id_end, tr_end, op, enc, summary = sess.run(
                        fetches=[self.opt, self.total_loss, self.adv_loss, self.perceptual_loss, self.gen_loss,
                                 self.id_loss_end, self.triplet_loss_end, output, image_enc, self.summary_op],
                        feed_dict=feed_dict_)
                    _, totloss, advloss, lo_per, lo_gen, op, enc = sess.run(
                        fetches=[self.opt_2, self.total_loss, self.adv_loss, self.perceptual_loss, self.gen_loss,
                                 output, image_enc], feed_dict=feed_dict_)
                    # _, totloss, advloss, lo_per, op, enc, summary = sess.run(
                    #     fetches=[self.opt, self.total_loss, self.adv_loss, self.perceptual_loss, output, image_enc,
                    #              self.summary_op], feed_dict=feed_dict_)
                    # _, totloss, advloss, lo_per, op, enc = sess.run(
                    #     fetches=[self.opt_2, self.total_loss, self.adv_loss, self.perceptual_loss, output, image_enc],
                    #     feed_dict=feed_dict_)
                    self.train_writer.add_summary(summary, self.iterations)
                    print(
                        '\r it: {} epochs: {} tot loss: {} adv loss: {} per_loss: {} gen_loss: {} id_loss_end: {} tr_loss_end: {}'.format(
                            self.iterations, ep, totloss, advloss, lo_per, lo_gen, id_end, tr_end), end="")
                    if np.mod(self.iterations, 100) == 0:
                        save(sess, filepath=os.path.join(model_load_path, 'weights.mdl'))
                        logging.info('Saved_model successfully at iteration %d' % self.iterations)
                    self.iterations += 1

    def test(self):
        self.degraded_image = tf.placeholder(dtype=tf.float32, shape=(None, 128, 128, 3))
        # Generator
        g = FARNet(self.params.batch_size, reuse=tf.AUTO_REUSE)
        # list for tower data
        tower_image_degraded = [self.degraded_image]
        gpu_id = 0
        for gpu_num in self.params.gpu:
            with tf.device(gpu_num):
                # Network G
                with tf.variable_scope('net_g'):
                    image_coarse = g.coarse_ar_net(tower_image_degraded[gpu_id], reuse=tf.AUTO_REUSE)
                    image_enc = g.fine_ar_enc(image_coarse, reuse=tf.AUTO_REUSE)
                    image_pmaps = g.prior_est_net(image_coarse, reuse=tf.AUTO_REUSE)
                    image_sum = image_enc + image_pmaps
                    output = g.fine_ar_dec(image_sum, reuse=tf.AUTO_REUSE)
                    # output = g.fine_ar_dec(image_enc, reuse=tf.AUTO_REUSE)
            gpu_id += 1
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            # ===== LOAD MODEL =====
            try:
                load(sess, os.path.join(model_load_path, 'weights.mdl'))
                print('Previous model loaded from %s' % model_load_path)
            except errors.InvalidArgumentError as e:
                logging.info(str(e) + 'No models found. Training from scratch...\n')
            except ValueError as e:
                logging.info(str(e) + 'No models found. Training from scratch...\n')
            except errors.NotFoundError as e:
                logging.info('Model of different architecture found. Aborting...\n' + str(e))
            # =======================
            self.iterations = 0
            img_per_batch = 1
            gen = DataGenerator(test_data_degraded)
            for img_batch_raw, filename in gen.yield_data_test_any(img_per_batch, extension='png'):
                feed_dict_ = {self.degraded_image: img_batch_raw}
                img = sess.run(fetches=[output],
                               feed_dict=feed_dict_)
                img = np.clip(img, a_max=1, a_min=0)
                for i in range(img_per_batch):
                    save_image((img[i, :, :, :] * 255).astype(np.uint8).squeeze(), test_data_enhanced + filename[i],
                               ext='')
                logging.debug('Example output images saved')
                # -------------------------
                print('\r iterations: {}'.format(self.iterations), end="")
                self.iterations += 1


if __name__ == '__main__':
    f = FaceHallucinate(param)
    try:
        f.train()
    except Exception as e:
        print(str(e) + '\n')
        print('\nSecond Epoch\n')
        f.train()
    f.test()
    print('Do not run. Other images present')
# from matplotlib import pyplot as plt
#
# plt.imshow(op[0, :, :, :])
# plt.show()
# python -m tensorboard.main --logdir=/home/soumya/Documents/front_face_generator/data/tensorboard

# import cv2
# gt_tf = cv2.imread('/media/soumya/HDD_1/testing_subset/ms_anyv_umd_MiddleAsian_renren_DEGRADED_mixed/00005/00005_000.jpg')
# gt_tf_2 = cv2.imread('/media/soumya/HDD_1/testing_subset/ms_anyv_umd_MiddleAsian_renren_DEGRADED_mixed/00005/00005_070.jpg')
# gt_tf = np.expand_dims(gt_tf, axis=0)
# gt_tf_2 = np.expand_dims(gt_tf_2, axis=0)
# aa = sess.run(fetches=self.net_anvv6.outputs, feed_dict={self.image: gt_tf})
# aa2 = sess.run(fetches=self.net_anvv6.outputs, feed_dict={self.image: gt_tf_2})
# from sklearn.metrics.pairwise import cosine_similarity
# acossim = cosine_similarity(aa, aa2)[0][0]

# import matplotlib.pyplot as plt
# pmp = pmaps[0,:,:,0]
# plt.clf()
# plt.imshow(pmp)
# plt.show()
