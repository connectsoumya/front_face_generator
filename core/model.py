import tensorflow as tf
from core.layers import conv2d
from core.layers import conv2d_transpose
from core.layers import lrelu
from core.layers import batchnorm
from core.layers import max_pool
from core.layers import upsampling_nn
from core.layers import dense
from core.layers import lrelu as relu
from utils.ops import sampler
from numpy import prod

# VGG_MEAN = [103.939, 116.779, 123.68]  # BGR
VGG_MEAN = [0.0, 0.0, 0.0]  # BGR


class Generator(object):

    def __init__(self, batch_size, num_classes, reuse):
        self.reuse = reuse
        self.batch_size = batch_size
        self.num_classes = num_classes

    def vgg19(self, image, reuse=False):
        print("build model started")
        # bgr_scaled = image * 255.0
        bgr_scaled = image
        # scale RGB image
        blue, green, red = tf.split(axis=3, num_or_size_splits=3, value=bgr_scaled)
        bgr = tf.concat(axis=3, values=[
            blue - VGG_MEAN[0],
            green - VGG_MEAN[1],
            red - VGG_MEAN[2],
        ])
        bgr = tf.subtract(tf.multiply(bgr, 2), 1)
        with tf.variable_scope('vgg_19', reuse=reuse):
            self.conv1_1 = lrelu(batchnorm(conv2d(bgr, filters=64, kernel_size=3, strides=1, name="conv1_1")))
            self.conv1_2 = lrelu(batchnorm(conv2d(self.conv1_1, filters=64, kernel_size=3, strides=1, name="conv1_2")))
            self.pool1 = max_pool(self.conv1_2, kernel_size=2, strides=2, name='pool1')

            self.conv2_1 = lrelu(batchnorm(conv2d(self.pool1, filters=128, kernel_size=3, strides=1, name="conv2_1")))
            self.conv2_2 = lrelu(batchnorm(conv2d(self.conv2_1, filters=128, kernel_size=3, strides=1, name="conv2_2")))
            self.pool2 = max_pool(self.conv2_2, kernel_size=2, strides=2, name='pool2')

            self.conv3_1 = lrelu(batchnorm(conv2d(self.pool2, filters=256, kernel_size=3, strides=1, name="conv3_1")))
            self.conv3_2 = lrelu(batchnorm(conv2d(self.conv3_1, filters=256, kernel_size=3, strides=1, name="conv3_2")))
            self.conv3_3 = lrelu(batchnorm(conv2d(self.conv3_2, filters=256, kernel_size=3, strides=1, name="conv3_3")))
            self.conv3_4 = lrelu(batchnorm(conv2d(self.conv3_3, filters=256, kernel_size=3, strides=1, name="conv3_4")))
            self.pool3 = max_pool(self.conv3_4, kernel_size=2, strides=2, name='pool3')

            self.conv4_1 = lrelu(batchnorm(conv2d(self.pool3, filters=512, kernel_size=3, strides=1, name="conv4_1")))
            self.conv4_2 = lrelu(batchnorm(conv2d(self.conv4_1, filters=512, kernel_size=3, strides=1, name="conv4_2")))
            self.conv4_3 = lrelu(batchnorm(conv2d(self.conv4_2, filters=512, kernel_size=3, strides=1, name="conv4_3")))
            self.conv4_4 = lrelu(batchnorm(conv2d(self.conv4_3, filters=512, kernel_size=3, strides=1, name="conv4_4")))
            self.pool4 = max_pool(self.conv4_4, kernel_size=2, strides=2, name='pool4')

            self.conv5_1 = lrelu(batchnorm(conv2d(self.pool4, filters=512, kernel_size=3, strides=1, name="conv5_1")))
            self.conv5_2 = lrelu(batchnorm(conv2d(self.conv5_1, filters=512, kernel_size=3, strides=1, name="conv5_2")))
            self.conv5_3 = lrelu(batchnorm(conv2d(self.conv5_2, filters=512, kernel_size=3, strides=1, name="conv5_3")))
            self.conv5_4 = lrelu(batchnorm(conv2d(self.conv5_3, filters=512, kernel_size=3, strides=1, name="conv5_4")))
            self.pool5 = max_pool(self.conv5_4, kernel_size=2, strides=2, name='pool5')

            shape = self.pool5.get_shape().as_list()  # a list: [None, 9, 2]
            dim = prod(shape[1:])
            reshape = tf.reshape(self.pool5, [-1, dim])
            self.fc6 = lrelu(dense(reshape, 4096, name='fc6'))
            self.face_vec = lrelu(dense(self.fc6, 4096, "fc7"))
            self.face_vec = lrelu(dense(self.face_vec, 256, "fc7ace"))
            self.fc8 = lrelu(dense(self.face_vec, self.num_classes, "fc8"))
            self.prob = tf.nn.softmax(self.fc8, name="prob")
        return self.face_vec, self.fc8

    def vgg19_vae(self, image, latent_dims=256, reuse=False):
        print("build model started")
        # bgr_scaled = image * 255.0
        bgr_scaled = image
        # scale RGB image
        blue, green, red = tf.split(axis=3, num_or_size_splits=3, value=bgr_scaled)
        bgr = tf.concat(axis=3, values=[
            blue - VGG_MEAN[0],
            green - VGG_MEAN[1],
            red - VGG_MEAN[2],
        ])
        bgr = tf.subtract(tf.multiply(bgr, 2), 1)
        with tf.variable_scope('vgg19_vae', reuse=reuse):
            self.conv1_1 = lrelu(batchnorm(conv2d(bgr, filters=64, kernel_size=3, strides=1, name="conv1_1")))
            self.conv1_2 = lrelu(batchnorm(conv2d(self.conv1_1, filters=64, kernel_size=3, strides=1, name="conv1_2")))
            self.pool1 = max_pool(self.conv1_2, kernel_size=2, strides=2, name='pool1')

            self.conv2_1 = lrelu(batchnorm(conv2d(self.pool1, filters=128, kernel_size=3, strides=1, name="conv2_1")))
            self.conv2_2 = lrelu(batchnorm(conv2d(self.conv2_1, filters=128, kernel_size=3, strides=1, name="conv2_2")))
            self.pool2 = max_pool(self.conv2_2, kernel_size=2, strides=2, name='pool2')

            self.conv3_1 = lrelu(batchnorm(conv2d(self.pool2, filters=256, kernel_size=3, strides=1, name="conv3_1")))
            self.conv3_2 = lrelu(batchnorm(conv2d(self.conv3_1, filters=256, kernel_size=3, strides=1, name="conv3_2")))
            self.conv3_3 = lrelu(batchnorm(conv2d(self.conv3_2, filters=256, kernel_size=3, strides=1, name="conv3_3")))
            self.conv3_4 = lrelu(batchnorm(conv2d(self.conv3_3, filters=256, kernel_size=3, strides=1, name="conv3_4")))
            self.pool3 = max_pool(self.conv3_4, kernel_size=2, strides=2, name='pool3')

            self.conv4_1 = lrelu(batchnorm(conv2d(self.pool3, filters=512, kernel_size=3, strides=1, name="conv4_1")))
            self.conv4_2 = lrelu(batchnorm(conv2d(self.conv4_1, filters=512, kernel_size=3, strides=1, name="conv4_2")))
            self.conv4_3 = lrelu(batchnorm(conv2d(self.conv4_2, filters=512, kernel_size=3, strides=1, name="conv4_3")))
            self.conv4_4 = lrelu(batchnorm(conv2d(self.conv4_3, filters=512, kernel_size=3, strides=1, name="conv4_4")))
            self.pool4 = max_pool(self.conv4_4, kernel_size=2, strides=2, name='pool4')

            self.conv5_1 = lrelu(batchnorm(conv2d(self.pool4, filters=512, kernel_size=3, strides=1, name="conv5_1")))
            self.conv5_2 = lrelu(batchnorm(conv2d(self.conv5_1, filters=512, kernel_size=3, strides=1, name="conv5_2")))
            self.conv5_3 = lrelu(batchnorm(conv2d(self.conv5_2, filters=512, kernel_size=3, strides=1, name="conv5_3")))
            self.conv5_4 = lrelu(batchnorm(conv2d(self.conv5_3, filters=512, kernel_size=3, strides=1, name="conv5_4")))
            self.pool5 = max_pool(self.conv5_4, kernel_size=2, strides=2, name='pool5')

            shape = self.pool5.get_shape().as_list()  # a list: [None, 9, 2]
            dim = prod(shape[1:])
            reshape = tf.reshape(self.pool5, [-1, dim])
            gaussian_params = dense(reshape, 2 * latent_dims, name='fc6')
            mu = gaussian_params[:, :latent_dims]
            var = gaussian_params[:, latent_dims:]
            sample = tf.distributions.Normal(loc=mu, scale=var)  # sampler(mu, var, latent_size=latent_dims)
            assert sample.reparameterization_type == tf.distributions.FULLY_REPARAMETERIZED
        return mu, var, sample

    def vgg19_inv(self, input_, reuse=False):
        with tf.variable_scope('vgg19_inv', reuse=reuse):
            self.ifc8 = lrelu(dense(input_, 4096, "ifc8"))
            self.ifc7 = lrelu(dense(self.ifc8, 4096, "ifc7"))
            self.ifc6 = lrelu(dense(self.ifc7, 8192, "ifc7"))
            reshape = tf.reshape(self.ifc6, [-1, 4, 4, 512])

            self.ipool5 = tf.image.resize_bilinear(reshape, (8, 8), name='ipool5')
            self.dconv5_4 = lrelu(
                batchnorm(conv2d_transpose(self.ipool5, filters=512, kernel_size=3, strides=1, name='dconv5_4')))
            self.dconv5_3 = lrelu(
                batchnorm(conv2d_transpose(self.dconv5_4, filters=512, kernel_size=3, strides=1, name='dconv5_3')))
            self.dconv5_2 = lrelu(
                batchnorm(conv2d_transpose(self.dconv5_3, filters=512, kernel_size=3, strides=1, name='dconv5_2')))
            self.dconv5_1 = lrelu(
                batchnorm(conv2d_transpose(self.dconv5_2, filters=512, kernel_size=3, strides=1, name='dconv5_1')))

            self.ipool4 = tf.image.resize_bilinear(self.dconv5_1, (16, 16), name='ipool4')
            self.dconv4_4 = lrelu(
                batchnorm(conv2d_transpose(self.ipool4, filters=512, kernel_size=3, strides=1, name='dconv4_4')))
            self.dconv4_3 = lrelu(
                batchnorm(conv2d_transpose(self.dconv4_4, filters=512, kernel_size=3, strides=1, name='dconv4_3')))
            self.dconv4_2 = lrelu(
                batchnorm(conv2d_transpose(self.dconv4_3, filters=512, kernel_size=3, strides=1, name='dconv4_2')))
            self.dconv4_1 = lrelu(
                batchnorm(conv2d_transpose(self.dconv4_2, filters=512, kernel_size=3, strides=1, name='dconv4_1')))

            self.ipool3 = tf.image.resize_bilinear(self.dconv4_1, (32, 32), name='ipool3')
            self.dconv3_4 = lrelu(
                batchnorm(conv2d_transpose(self.ipool3, filters=256, kernel_size=3, strides=1, name='dconv3_4')))
            self.dconv3_3 = lrelu(
                batchnorm(conv2d_transpose(self.dconv3_4, filters=256, kernel_size=3, strides=1, name='dconv3_3')))
            self.dconv3_2 = lrelu(
                batchnorm(conv2d_transpose(self.dconv3_3, filters=256, kernel_size=3, strides=1, name='dconv3_2')))
            self.dconv3_1 = lrelu(
                batchnorm(conv2d_transpose(self.dconv3_2, filters=256, kernel_size=3, strides=1, name='dconv3_1')))

            self.ipool2 = tf.image.resize_bilinear(self.dconv3_1, (64, 64), name='ipool2')
            self.dconv2_2 = lrelu(
                batchnorm(conv2d_transpose(self.ipool2, filters=128, kernel_size=3, strides=1, name='dconv2_2')))
            self.dconv2_1 = lrelu(
                batchnorm(conv2d_transpose(self.dconv2_2, filters=128, kernel_size=3, strides=1, name='dconv2_1')))

            self.ipool1 = tf.image.resize_bilinear(self.dconv2_1, (128, 128), name='ipool1')
            self.dconv1_2 = lrelu(
                batchnorm(conv2d_transpose(self.ipool1, filters=64, kernel_size=3, strides=1, name='dconv1_2')))
            self.dconv1_1 = lrelu(
                batchnorm(conv2d_transpose(self.dconv1_2, filters=64, kernel_size=3, strides=1, name='dconv1_1')))

            bgr = tf.tanh(conv2d(self.dconv1_1, filters=3, kernel_size=1, strides=1, name='output_raw'))

        # scale RGB image
        blue, green, red = tf.split(axis=3, num_or_size_splits=3, value=bgr)
        bgr_scaled = tf.concat(axis=3, values=[
            blue + VGG_MEAN[0],
            green + VGG_MEAN[1],
            red + VGG_MEAN[2],
        ])

        # image = bgr_scaled / 255
        image = bgr_scaled
        return image


class Discriminator(object):

    def __init__(self, batch_size):
        self.batch_size = batch_size

    def dcgan(self, image, reuse=False):
        with tf.variable_scope('discriminator', reuse=reuse):
            h0 = lrelu(conv2d(image, filters=64, kernel_size=5, strides=2, name='d_h0_conv'))
            h1 = lrelu(batchnorm(conv2d(h0, filters=128, kernel_size=5, strides=2, name='d_h1_conv')))
            h2 = lrelu(batchnorm(conv2d(h1, filters=256, kernel_size=5, strides=2, name='d_h2_conv')))
            h3 = lrelu(batchnorm(conv2d(h2, filters=512, kernel_size=5, strides=2, name='d_h3_conv')))

            shape = h3.get_shape().as_list()  # a list: [None, 9, 2]
            dim = prod(shape[1:])
            reshape = tf.reshape(h3, [-1, dim])
            h4 = dense(reshape, 1, name='d_h4_lin')
            return h4  # tf.nn.sigmoid(h4)


class FSRNet(object):
    def __init__(self, batch_size, reuse):
        self.reuse = reuse
        self.batch_size = batch_size

    def _residual_block(self, _input, filters=64, kernel=3, strides=1):
        with tf.variable_scope('conv_1_res'):
            x1 = conv2d(_input, filters, kernel, strides, name='conv')
            x1 = batchnorm(x1)
            x1 = relu(x1)
        with tf.variable_scope('conv_2_res'):
            x2 = conv2d(x1, filters, kernel, strides, name='conv')
            x2 = batchnorm(x2)
            x2 = relu(x2)
            x2 = _input + x2
        return x2

    def hour_glass_res(self, _input):
        with tf.variable_scope('res_1'):
            x1 = self._residual_block(_input, 128, 1, 1)  # 128
            x1_br = conv2d(x1, 128, 1, 1, name='conv')
            x1 = max_pool(x1, 2, 2)
        with tf.variable_scope('res_2'):
            x2 = self._residual_block(x1, 128, 1, 1)  # 64
            x2_br = conv2d(x2, 128, 1, 1, name='conv')
            x2 = max_pool(x2, 2, 2)
        with tf.variable_scope('res_3'):
            x3 = self._residual_block(x2, 128, 1, 1)  # 32
            x3_br = conv2d(x3, 128, 1, 1, name='conv')
            x3 = max_pool(x3, 2, 2)
        with tf.variable_scope('res_4'):
            x4 = self._residual_block(x3, 128, 1, 1)  # 16
            x4_br = conv2d(x4, 128, 1, 1, name='conv')
            x4 = max_pool(x4, 2, 2)
        with tf.variable_scope('res_5'):
            x5 = self._residual_block(x4, 128, 1, 1)  # 8
            x5 = upsampling_nn(x5, 2)
            x5 += x4_br
        with tf.variable_scope('res_6'):
            x6 = self._residual_block(x5, 128, 1, 1)  # 16
            x6 = upsampling_nn(x6, 2)
            x6 += x3_br
        with tf.variable_scope('res_7'):
            x7 = self._residual_block(x6, 128, 1, 1)  # 32
            x7 = upsampling_nn(x7, 2)
            x7 += x2_br
        with tf.variable_scope('res_8'):
            x8 = self._residual_block(x7, 128, 1, 1)  # 64
            x8 = upsampling_nn(x8, 2)
            x8 += x1_br
        with tf.variable_scope('res_9'):
            x9 = self._residual_block(x8, 128, 1, 1)
        return x9

    def hour_glass(self, _input):
        with tf.variable_scope('res_1'):
            x1 = conv2d(_input, 128, 1, 1, name='conv_')  # 128
            x1_br = conv2d(x1, 128, 1, 1, name='conv')
            x1 = max_pool(x1, 2, 2)
        with tf.variable_scope('res_2'):
            x2 = conv2d(x1, 128, 1, 1, name='conv_')  # 64
            x2_br = conv2d(x2, 128, 1, 1, name='conv')
            x2 = max_pool(x2, 2, 2)
        with tf.variable_scope('res_3'):
            x3 = conv2d(x2, 128, 1, 1, name='conv_')  # 32
            x3_br = conv2d(x3, 128, 1, 1, name='conv')
            x3 = max_pool(x3, 2, 2)
        with tf.variable_scope('res_4'):
            x4 = conv2d(x3, 128, 1, 1, name='conv_')  # 16
            x4_br = conv2d(x4, 128, 1, 1, name='conv')
            x4 = max_pool(x4, 2, 2)
        with tf.variable_scope('res_5'):
            x5 = conv2d(x4, 128, 1, 1, name='conv')  # 8
            x5 = upsampling_nn(x5, 2)
            x5 += x4_br
        with tf.variable_scope('res_6'):
            x6 = conv2d(x5, 128, 1, 1, name='conv')  # 16
            x6 = upsampling_nn(x6, 2)
            x6 += x3_br
        with tf.variable_scope('res_7'):
            x7 = conv2d(x6, 128, 1, 1, name='conv')  # 32
            x7 = upsampling_nn(x7, 2)
            x7 += x2_br
        with tf.variable_scope('res_8'):
            x8 = conv2d(x7, 128, 1, 1, name='conv')  # 64
            x8 = upsampling_nn(x8, 2)
            x8 += x1_br
        with tf.variable_scope('res_9'):
            x9 = conv2d(x8, 128, 1, 1, name='conv')
        return x9

    def coarse_ar_net(self, image):
        with tf.variable_scope('coarse_sr_net'):
            with tf.variable_scope('conv_1'):
                x = conv2d(image, 64, 3, 1, name='conv')
                x = batchnorm(x)
                x = relu(x)
            with tf.variable_scope('res_1'):
                x = self._residual_block(x)
            with tf.variable_scope('res_2'):
                x = self._residual_block(x)
            with tf.variable_scope('res_3'):
                x = self._residual_block(x)
            with tf.variable_scope('conv_8'):
                xout = conv2d(x, 3, 3, 1, name='conv')
        return xout

    def fine_ar_enc(self, image):
        with tf.variable_scope('fine_sr_enc'):
            with tf.variable_scope('conv_1'):
                x = conv2d(image, 64, 3, 1, name='conv')
                x = batchnorm(x)
                x = relu(x)
            with tf.variable_scope('res_1'):
                x = self._residual_block(x)
            with tf.variable_scope('res_2'):
                x = self._residual_block(x)
            with tf.variable_scope('res_3'):
                x = self._residual_block(x)
            with tf.variable_scope('res_4'):
                x = self._residual_block(x)
            with tf.variable_scope('res_5'):
                x = self._residual_block(x)
            with tf.variable_scope('res_6'):
                x = self._residual_block(x)
            with tf.variable_scope('res_7'):
                x = self._residual_block(x)
            with tf.variable_scope('res_8'):
                x = self._residual_block(x)
            with tf.variable_scope('res_9'):
                x = self._residual_block(x)
            with tf.variable_scope('res_10'):
                x = self._residual_block(x)
            with tf.variable_scope('res_11'):
                x = self._residual_block(x)
            with tf.variable_scope('res_12'):
                x = self._residual_block(x)
            with tf.variable_scope('conv_2'):
                xout = conv2d(x, 64, 3, 1, name='conv')
        return xout

    def prior_est_net(self, image):
        with tf.variable_scope('prior_est_net'):
            with tf.variable_scope('conv_1'):
                x = conv2d(image, 128, 7, 1, name='conv')
                x = batchnorm(x)
                x = relu(x)
            with tf.variable_scope('res_1'):
                x = self._residual_block(x, filters=128, kernel=3, strides=1)
            with tf.variable_scope('res_2'):
                x = self._residual_block(x, filters=128, kernel=3, strides=1)
            with tf.variable_scope('res_3'):
                x = self._residual_block(x, filters=128, kernel=3, strides=1)
            with tf.variable_scope('hg_1'):
                x = self.hour_glass(x)
            with tf.variable_scope('conv_2'):
                x = conv2d(x, 11, 1, 1, name='conv')
        return x

    def fine_ar_dec(self, image):
        with tf.variable_scope('fine_sr_dec'):
            with tf.variable_scope('conv_1'):
                x = conv2d(image, 64, 3, 1, name='conv')
                x = batchnorm(x)
                x = relu(x)
            with tf.variable_scope('deconv_1'):
                x = conv2d_transpose(x, 64, 3, 1)
            with tf.variable_scope('res_1'):
                x = self._residual_block(x)
            with tf.variable_scope('res_2'):
                x = self._residual_block(x)
            with tf.variable_scope('res_3'):
                x = self._residual_block(x)
            with tf.variable_scope('conv_2'):
                xout = conv2d(x, 3, 3, 1, name='conv')
        return xout

    # def network(self, image):
    #     image_coarse = self.coarse_sr_net(image)
    #     image_enc = self.fine_sr_enc(image_coarse)
    #     image_lm = self.prior_est_net(image_coarse)
    #     image_concat = tf.concat([image_enc, image_lm], axis=-1)
    #     output = self.fine_sr_dec(image_concat)
    #     return output


class FARNet(object):
    def __init__(self, batch_size, reuse):
        self.reuse = reuse
        self.batch_size = batch_size

    def _residual_block(self, _input, filters=64, kernel=3, strides=1):
        with tf.variable_scope('conv_1_res'):
            x1 = conv2d(_input, filters, kernel, strides, name='conv')
            x1 = batchnorm(x1)
            x1 = relu(x1)
        with tf.variable_scope('conv_2_res'):
            x2 = conv2d(x1, filters, kernel, strides, name='conv')
            x2 = batchnorm(x2)
            x2 = relu(x2)
            x2 = _input + x2
        return x2

    def hour_glass_res(self, _input):
        with tf.variable_scope('res_1'):
            x1 = self._residual_block(_input, 128, 1, 1)  # 128
            x1_br = conv2d(x1, 128, 1, 1, name='conv')
            x1 = max_pool(x1, 2, 2)
        with tf.variable_scope('res_2'):
            x2 = self._residual_block(x1, 128, 1, 1)  # 64
            x2_br = conv2d(x2, 128, 1, 1, name='conv')
            x2 = max_pool(x2, 2, 2)
        with tf.variable_scope('res_3'):
            x3 = self._residual_block(x2, 128, 1, 1)  # 32
            x3_br = conv2d(x3, 128, 1, 1, name='conv')
            x3 = max_pool(x3, 2, 2)
        with tf.variable_scope('res_4'):
            x4 = self._residual_block(x3, 128, 1, 1)  # 16
            x4_br = conv2d(x4, 128, 1, 1, name='conv')
            x4 = max_pool(x4, 2, 2)
        with tf.variable_scope('res_5'):
            x5 = self._residual_block(x4, 128, 1, 1)  # 8
            x5 = upsampling_nn(x5, 2)
            x5 += x4_br
        with tf.variable_scope('res_6'):
            x6 = self._residual_block(x5, 128, 1, 1)  # 16
            x6 = upsampling_nn(x6, 2)
            x6 += x3_br
        with tf.variable_scope('res_7'):
            x7 = self._residual_block(x6, 128, 1, 1)  # 32
            x7 = upsampling_nn(x7, 2)
            x7 += x2_br
        with tf.variable_scope('res_8'):
            x8 = self._residual_block(x7, 128, 1, 1)  # 64
            x8 = upsampling_nn(x8, 2)
            x8 += x1_br
        with tf.variable_scope('res_9'):
            x9 = self._residual_block(x8, 128, 1, 1)
        return x9

    def hour_glass(self, _input):
        with tf.variable_scope('res_1'):
            x1 = conv2d(_input, 128, 1, 1, name='conv_')  # 128
            x1_br = conv2d(x1, 128, 1, 1, name='conv')
            x1 = max_pool(x1, 2, 2)
        with tf.variable_scope('res_2'):
            x2 = conv2d(x1, 128, 1, 1, name='conv_')  # 64
            x2_br = conv2d(x2, 128, 1, 1, name='conv')
            x2 = max_pool(x2, 2, 2)
        with tf.variable_scope('res_3'):
            x3 = conv2d(x2, 128, 1, 1, name='conv_')  # 32
            x3_br = conv2d(x3, 128, 1, 1, name='conv')
            x3 = max_pool(x3, 2, 2)
        with tf.variable_scope('res_4'):
            x4 = conv2d(x3, 128, 1, 1, name='conv_')  # 16
            x4_br = conv2d(x4, 128, 1, 1, name='conv')
            x4 = max_pool(x4, 2, 2)
        with tf.variable_scope('res_5'):
            x5 = conv2d(x4, 128, 1, 1, name='conv')  # 8
            x5 = upsampling_nn(x5, 2)
            x5 += x4_br
        with tf.variable_scope('res_6'):
            x6 = conv2d(x5, 128, 1, 1, name='conv')  # 16
            x6 = upsampling_nn(x6, 2)
            x6 += x3_br
        with tf.variable_scope('res_7'):
            x7 = conv2d(x6, 128, 1, 1, name='conv')  # 32
            x7 = upsampling_nn(x7, 2)
            x7 += x2_br
        with tf.variable_scope('res_8'):
            x8 = conv2d(x7, 128, 1, 1, name='conv')  # 64
            x8 = upsampling_nn(x8, 2)
            x8 += x1_br
        with tf.variable_scope('res_9'):
            x9 = conv2d(x8, 128, 1, 1, name='conv')
        return x9

    def coarse_ar_net(self, image, reuse):
        with tf.variable_scope('coarse_sr_net', reuse=reuse):
            with tf.variable_scope('conv_1'):
                x = conv2d(image, 64, 3, 1, name='conv')
                x = batchnorm(x)
                x = relu(x)
            with tf.variable_scope('res_1'):
                x = self._residual_block(x)
            with tf.variable_scope('res_2'):
                x = self._residual_block(x)
            with tf.variable_scope('res_3'):
                x = self._residual_block(x)
            with tf.variable_scope('conv_8'):
                xout = conv2d(x, 3, 3, 1, name='conv')
        return xout

    def fine_ar_enc(self, image, reuse):
        with tf.variable_scope('fine_sr_enc', reuse=reuse):
            with tf.variable_scope('conv_1'):
                x = conv2d(image, 64, 3, 1, name='conv')
                x = batchnorm(x)
                x = relu(x)
            with tf.variable_scope('res_1'):
                x = self._residual_block(x)
            with tf.variable_scope('res_2'):
                x = self._residual_block(x)
            with tf.variable_scope('res_3'):
                x = self._residual_block(x)
            with tf.variable_scope('res_4'):
                x = self._residual_block(x)
            with tf.variable_scope('res_5'):
                x = self._residual_block(x)
            with tf.variable_scope('res_6'):
                x = self._residual_block(x)
            with tf.variable_scope('res_7'):
                x = self._residual_block(x)
            with tf.variable_scope('res_8'):
                x = self._residual_block(x)
            with tf.variable_scope('res_9'):
                x = self._residual_block(x)
            with tf.variable_scope('res_10'):
                x = self._residual_block(x)
            with tf.variable_scope('res_11'):
                x = self._residual_block(x)
            with tf.variable_scope('res_12'):
                x = self._residual_block(x)
            with tf.variable_scope('conv_2'):
                xout = conv2d(x, 64, 3, 1, name='conv')
        return xout

    def prior_est_net(self, image, reuse):
        with tf.variable_scope('prior_est_net', reuse=reuse):
            with tf.variable_scope('conv_1'):
                x = conv2d(image, 128, 7, 1, name='conv')
                x = batchnorm(x)
                x = relu(x)
            with tf.variable_scope('res_1'):
                x = self._residual_block(x, filters=128, kernel=3, strides=1)
            with tf.variable_scope('res_2'):
                x = self._residual_block(x, filters=128, kernel=3, strides=1)
            with tf.variable_scope('res_3'):
                x = self._residual_block(x, filters=128, kernel=3, strides=1)
            with tf.variable_scope('hg_1'):
                x = self.hour_glass(x)
            with tf.variable_scope('conv_2'):
                x = conv2d(x, 1, 1, 1, name='conv')
        return x

    def fine_ar_dec(self, image, reuse):
        with tf.variable_scope('fine_sr_dec', reuse=reuse):
            with tf.variable_scope('conv_1'):
                x = conv2d(image, 64, 3, 1, name='conv')
                x = batchnorm(x)
                x = relu(x)
            # with tf.variable_scope('deconv_1'):
            #     x = conv2d_transpose(x, 64, 3, 1)
            with tf.variable_scope('res_1'):
                x = self._residual_block(x)
            with tf.variable_scope('res_2'):
                x = self._residual_block(x)
            with tf.variable_scope('res_3'):
                x = self._residual_block(x)
            with tf.variable_scope('conv_2'):
                xout = conv2d(x, 3, 3, 1, name='conv')
        return xout

    # def network(self, image):
    #     image_coarse = self.coarse_sr_net(image)
    #     image_enc = self.fine_sr_enc(image_coarse)
    #     image_lm = self.prior_est_net(image_coarse)
    #     image_concat = tf.concat([image_enc, image_lm], axis=-1)
    #     output = self.fine_sr_dec(image_concat)
    #     return output


if __name__ == '__main__':
    im = tf.placeholder(dtype=tf.float32, shape=(None, 128, 128, 3))
    f = FSRNet(8, True)
    f.network(im)
