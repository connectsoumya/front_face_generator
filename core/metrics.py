from __future__ import print_function
from __future__ import division
import os
import numpy
from scipy import signal
import sys
from os.path import dirname, abspath
import logging.config
from skimage.filters import prewitt
from skimage.measure import compare_ssim
import numpy as np
from other_algos.canny import TF_Canny
# from core.vgg19api import vgg19_simple_api
# from reco_models.anv_v6_tf_api import anv_v6_api

try:
    os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2"
    import tensorflow as tf

    is_tensorflow_available = True
except ImportError:
    is_tensorflow_available = False

from configparser import ConfigParser

parser = ConfigParser()
d = dirname(dirname(dirname(abspath(__file__))))
parser.read('config.ini')
logging.config.fileConfig('logging.ini')


def psnr(img_1, img_2):
    """
    Measure the Peak Signal to Noise Ratio between two images.
    :param img_1: Image 1
    :param img_2: Image 2
    # :type img_1: np.ndarray
    # :type img_2: np.ndarray
    :return: PSNR
    """
    mse = np.mean((img_1 - img_2) ** 2)
    if mse == 0:
        return 100
    pixel_max = 1.0
    return 20.0 * np.log10(pixel_max / np.sqrt(mse))


def ssim(img_1, img_2, multichannel=True, **kwargs):
    """
    Measure the structured similarity index between two images. Refer to
    http://scikit-image.org/docs/dev/api/skimage.measure.html#skimage.measure.compare_ssim
    :param img_1: The original image
    :param img_2: The image to be compared to the original image
    # :type img_1: np.ndarray
    # :type img_2: np.ndarray
    :param kwargs:
    :return: SSIM
    """
    mssim = 0
    if len(img_1.shape) == 3:
        return compare_ssim(img_1, img_2, data_range=img_2.max() - img_2.min(), multichannel=multichannel, **kwargs)
    else:
        for i in range(img_1.shape[0]):
            im1 = img_1[i, :, :, :]
            im2 = img_2[i, :, :, :]
            mssim += compare_ssim(im1, im2, data_range=im2.max() - im2.min(), multichannel=multichannel, **kwargs)
        return mssim / img_1.shape[0]


def exp_loss(x_pred, x_real):
    """

    :param x_pred:
    :param x_real:
    :return:
    """
    l2 = tf.reduce_mean(tf.exp(tf.abs(x_pred - x_real)))
    return l2


def rgb2gray(rgb):
    h = 128
    r = tf.slice(rgb, [0, 0, 0, 0], [tf.shape(rgb)[0], h, h, 1])
    g = tf.slice(rgb, [0, 0, 0, 1], [tf.shape(rgb)[0], h, h, 1])
    b = tf.slice(rgb, [0, 0, 0, 2], [tf.shape(rgb)[0], h, h, 1])
    gray = tf.multiply(0.2989, r) + tf.multiply(0.5870, g) + tf.multiply(0.1140, b)
    return gray


def canny_loss(x_pred, x_real, raw_edge=False):
    x_pred_tensor = rgb2gray(x_pred)
    x_real_tensor = rgb2gray(x_real)
    edges_tensor_pred = TF_Canny(x_pred_tensor, return_raw_edges=raw_edge)
    edges_tensor_real = TF_Canny(x_real_tensor, return_raw_edges=raw_edge)
    return l1_loss(edges_tensor_pred, edges_tensor_real)


# def vgg_loss(x_pred, x_real):
#     net_vgg, vgg_target_emb = vgg19_simple_api((x_real + 1) / 2, reuse=tf.AUTO_REUSE)
#     _, vgg_predict_emb = vgg19_simple_api((x_pred + 1) / 2, reuse=True)
#
#     def vggloss_func(list1, list2):
#         channel_norm = tf.norm(list1[0].outputs - list2[0].outputs, ord='euclidean', axis=[1, 2])
#         return tf.reduce_mean(tf.reduce_mean(channel_norm, axis=1))
#
#     vgg_loss = vggloss_func(vgg_predict_emb, vgg_target_emb)
#     return net_vgg, vgg_loss


def cosine_similarity(x_pred, x_real):
    normalize_a = tf.nn.l2_normalize(x_pred, 1)
    normalize_b = tf.nn.l2_normalize(x_real, 1)
    cos_similarity = tf.reduce_sum(tf.multiply(normalize_a, normalize_b), axis=1)
    return tf.reduce_mean(cos_similarity)


def cosine_distance(x_pred, x_real):
    normalize_a = tf.nn.l2_normalize(x_pred, 1)
    normalize_b = tf.nn.l2_normalize(x_real, 1)
    cos_similarity = tf.reduce_sum(tf.multiply(normalize_a, normalize_b), axis=1)
    return tf.reduce_mean(1 - cos_similarity)


# def face_id_loss(x_pred, x_real):
#     x_pred = (x_pred + 1) / 2
#     x_real = (x_real + 1) / 2
#     real_feat = anv_v6_api(x_real, reuse=tf.AUTO_REUSE)
#     fake_feat = anv_v6_api(x_pred, reuse=tf.AUTO_REUSE)
#     return real_feat, 1 - cosine_similarity(real_feat.outputs, fake_feat.outputs)
#
#
# def canny_vgg_loss(x_pred, x_real, raw_edge=False, canny_ratio=0.4):
#     canny = canny_loss(x_pred, x_real, raw_edge)
#     net_vgg, vgg = vgg_loss(x_pred, x_real)
#     return net_vgg, canny_ratio * canny + (1 - canny_ratio) * vgg, canny, vgg


# def vgg_id_loss(x_pred, x_real, x_pred_logit, id_ratio=0.5):
#     v6_net, id_loss = face_id_loss(x_pred, x_real)
#     vgg_net, loss_vgg = vgg_loss(x_pred_logit, x_real)
#     return v6_net, vgg_net, id_ratio * id_loss + (1 - id_ratio) * loss_vgg, id_loss, loss_vgg


def l1_loss(x_pred, x_real):
    l1 = tf.reduce_mean(tf.abs(x_pred - x_real))
    return l1


def mse_loss(x_pred, x_real):
    mse = tf.reduce_mean(tf.abs(x_pred - x_real) ** 2)
    return mse


def vgg_mse(x_pred, x_real, vgg_weight=0.01):
    vgg_net, loss_vgg = vgg_loss(x_pred, x_real)
    mse = mse_loss(x_pred, x_real)
    total_loss = vgg_weight * loss_vgg + mse
    return vgg_net, total_loss, mse, loss_vgg


def gmsd(x_pred, x_real, c=0.0026):
    """
    https://arxiv.org/pdf/1308.3052.pdf
    :param x_pred:
    :param x_real:
    :param c:
    :return:
    """
    gms = _gms(x_pred, x_real, c)
    gmsm = _gmsm(gms)
    return np.sqrt(np.mean((gms - gmsm) ** 2))


def _gms(x_pred, x_real, c):
    """

    :rtype: np.ndarray
    """

    def rgb2gray(rgb):
        r, g, b = rgb[:, :, 0], rgb[:, :, 1], rgb[:, :, 2]
        gray = 0.2989 * r + 0.5870 * g + 0.1140 * b
        return gray

    grad_pred = np.zeros(shape=x_pred.shape[:3])
    grad_real = np.zeros(shape=x_real.shape[:3])
    if len(x_pred.shape) == 4 and x_pred.shape[-1] == 3:
        for i in range(x_pred.shape[0]):
            grad_pred[i, :, :] = prewitt(rgb2gray(x_pred[i, :, :, :]))
            grad_real[i, :, :] = prewitt(rgb2gray(x_real[i, :, :, :]))
    elif len(x_pred.shape) == 3 and x_pred.shape[-1] == 3:
        grad_pred = prewitt(rgb2gray(x_pred))
        grad_real = prewitt(rgb2gray(x_real))
    elif len(x_pred.shape) == 4 and x_pred.shape[-1] == 1:
        for i in range(x_pred.shape[0]):
            grad_pred[i, :, :] = prewitt(x_pred[i, :, :, 0])
            grad_real[i, :, :] = prewitt(x_real[i, :, :, 0])
    elif len(x_pred.shape) == 3 and x_pred.shape[-1] == 1:
        grad_pred = prewitt(x_pred)
        grad_real = prewitt(x_real)
    else:
        raise ValueError('Check the function in the source code. You might need to develop this condition.')

    gms = (2 * grad_real * grad_pred + c) / (grad_real ** 2 + grad_pred ** 2 + c)
    return gms


def triplet_loss(anchor, positive, negative, alpha):
    """Calculate the triplet loss according to the FaceNet paper

    Args:
      anchor: the embeddings for the anchor images.
      positive: the embeddings for the positive images.
      negative: the embeddings for the negative images.

    Returns:
      the triplet loss according to the FaceNet paper as a float tensor.
    """
    with tf.variable_scope('triplet_loss'):
        pos_dist = tf.losses.cosine_distance(anchor, positive, axis=1, reduction=tf.losses.Reduction.NONE)
        neg_dist = tf.losses.cosine_distance(anchor, negative, axis=1, reduction=tf.losses.Reduction.NONE)
        basic_loss = tf.add(tf.subtract(pos_dist, neg_dist), alpha)
        loss = tf.reduce_mean(basic_loss, 0)
    return loss

def lossless_triplet_loss(anchor, positive, negative, alpha=1.0000001):
    """Calculate the triplet loss according to the FaceNet paper

    Args:
      anchor: the embeddings for the anchor images.
      positive: the embeddings for the positive images.
      negative: the embeddings for the negative images.

    Returns:
      the triplet loss according to the FaceNet paper as a float tensor.
    """
    with tf.variable_scope('triplet_loss'):
        pos_dist = cosine_distance(anchor, positive)
        neg_dist = cosine_distance(anchor, negative)
        # adding the non-linearity
        pos_dist = tf.add(pos_dist, alpha)
        neg_dist = tf.add(neg_dist, alpha)
        # total loss
        total_loss = tf.subtract(pos_dist, neg_dist)
        loss = tf.reduce_mean(total_loss)
    return loss


# def vgg_triplet_loss(x_pred, x_real, anchor, id_ratio, triplet_alpha):
#     net_vgg, vgg_l = vgg_loss(x_pred, x_real)
#     net_v6 = anv_v6_api((x_pred + 1) / 2, reuse=tf.AUTO_REUSE)
#     pos_feat = net_v6.outputs
#     pos_feat_real = anv_v6_api((x_real + 1) / 2, reuse=tf.AUTO_REUSE).outputs
#     neg_feat = tf.manip.roll(pos_feat_real, shift=1, axis=0)
#     triplet_l = triplet_loss(anchor=anchor, positive=pos_feat, negative=neg_feat, alpha=triplet_alpha)
#     total_loss = tf.add(tf.multiply((1 - id_ratio), vgg_l), tf.multiply(id_ratio, triplet_l))
#     return net_v6, net_vgg, total_loss, triplet_l, vgg_l


# def vgg_lossless_triplet_loss(x_pred, x_real, anchor, id_ratio, triplet_alpha):
#     net_vgg, vgg_l = vgg_loss(x_pred, x_real)
#     net_v6 = anv_v6_api((x_pred + 1) / 2, reuse=tf.AUTO_REUSE)
#     pos_feat = net_v6.outputs
#     pos_feat_real = anv_v6_api((x_real + 1) / 2, reuse=tf.AUTO_REUSE).outputs
#     neg_feat = tf.manip.roll(pos_feat_real, shift=1, axis=0)
#     triplet_l = lossless_triplet_loss(anchor=anchor, positive=pos_feat, negative=neg_feat, alpha=triplet_alpha)
#     total_loss = tf.add(tf.multiply(id_ratio, vgg_l), triplet_l)
#     return net_v6, net_vgg, total_loss, triplet_l, vgg_l


def center_loss(features, label, alfa=0.5, nrof_classes=14528):
    """Center loss based on the paper "A Discriminative Feature Learning Approach for Deep Face Recognition"
       (http://ydwen.github.io/papers/WenECCV16.pdf)
    """

    nrof_features = features.get_shape()[1]
    centers = tf.get_variable('centers', [nrof_classes, nrof_features], dtype=tf.float32,
                              initializer=tf.random_normal_initializer(1.0, 0.02), trainable=True)
    label = tf.reshape(label, [-1])
    centers_batch = tf.gather(centers, label)
    loss = tf.nn.l2_loss(features - centers_batch)
    diff = centers_batch - features
    unique_label, unique_idx, unique_count = tf.unique_with_counts(label)
    appear_times = tf.gather(unique_count, unique_idx)
    appear_times = tf.reshape(appear_times, [-1, 1])
    diff = diff / tf.cast((1 + appear_times), tf.float32)
    diff = alfa * diff
    centers = tf.scatter_sub(centers, label, diff)
    return loss, centers


def arcface_loss(embedding, labels, out_num, w_init=None, s=64., m=0.5):
    '''
    :param embedding: the input embedding vectors
    :param labels:  the input labels, the shape should be eg: (batch_size, 1)
    :param s: scalar value default is 64
    :param out_num: output class num
    :param m: the margin value, default is 0.5
    :return: the final calcualted output, this output is send into the tf.nn.softmax directly
    '''
    cos_m = np.cos(m)
    sin_m = np.sin(m)
    mm = sin_m * m  # issue 1
    threshold = np.cos(np.pi - m)
    with tf.variable_scope('arcface_loss'):
        # inputs and weights norm
        embedding_norm = tf.norm(embedding, axis=1, keep_dims=True)
        embedding = tf.div(embedding, embedding_norm, name='norm_embedding')
        weights = tf.get_variable(name='embedding_weights', shape=(embedding.get_shape().as_list()[-1], out_num),
                                  initializer=w_init, dtype=tf.float32)
        weights_norm = tf.norm(weights, axis=0, keep_dims=True)
        weights = tf.div(weights, weights_norm, name='norm_weights')
        # cos(theta+m)
        cos_t = tf.matmul(embedding, weights, name='cos_t')
        cos_t2 = tf.square(cos_t, name='cos_2')
        sin_t2 = tf.subtract(1., cos_t2, name='sin_2')
        sin_t = tf.sqrt(sin_t2, name='sin_t')
        cos_mt = s * tf.subtract(tf.multiply(cos_t, cos_m), tf.multiply(sin_t, sin_m), name='cos_mt')

        # this condition controls the theta+m should in range [0, pi]
        #      0<=theta+m<=pi
        #     -m<=theta<=pi-m
        cond_v = cos_t - threshold
        cond = tf.cast(tf.nn.relu(cond_v, name='if_else'), dtype=tf.bool)

        keep_val = s * (cos_t - mm)
        cos_mt_temp = tf.where(cond, cos_mt, keep_val)

        mask = tf.one_hot(labels, depth=out_num, name='one_hot_mask')
        # mask = tf.squeeze(mask, 1)
        inv_mask = tf.subtract(1., mask, name='inverse_mask')

        s_cos_t = tf.multiply(s, cos_t, name='scalar_cos_t')

        output = tf.add(tf.multiply(s_cos_t, inv_mask), tf.multiply(cos_mt_temp, mask), name='arcface_loss_output')
    return output


def haarpsi(x_pred, x_real, preprocess_with_subsampling=True):
    haar = []
    if len(x_pred.shape) == 4 and x_pred.shape[-1] == 3:
        for i in range(x_pred.shape[0]):
            haar.append(_haar_psi(x_pred[i, :, :, :], x_real[i, :, :, :], preprocess_with_subsampling)[0])
    elif len(x_pred.shape) == 3 and x_pred.shape[-1] == 3:
        haar = _haar_psi(x_pred, x_real, preprocess_with_subsampling[0])
    elif len(x_pred.shape) == 4 and x_pred.shape[-1] == 1:
        for i in range(x_pred.shape[0]):
            haar.append(_haar_psi(x_pred[i, :, :, 0], x_real[i, :, :, 0], preprocess_with_subsampling)[0])
    elif len(x_pred.shape) == 3 and x_pred.shape[-1] == 1:
        haar = _haar_psi(x_pred[:, :, 0], x_real[:, :, 0], preprocess_with_subsampling[0])
    else:
        raise ValueError('Check the function in the source code. You might need to develop this condition.')
    return np.mean(haar)


def _gmsm(gms):
    return np.mean(gms)


def _haar_psi(reference_image, distorted_image, preprocess_with_subsampling=True):
    """
    Calculates the HaarPSI perceptual similarity index between the two specified images.

    Parameters:
    -----------
        reference_image: numpy.ndarray | tensorflow.Tensor | tensorflow.Variable
            The reference image, which can be in RGB or grayscale. The values must be in the range [0, 255].
            The image must be a NumPy array or TensorFlow tensor of the shape (width, height, 3) in the case
            of RGB, or a NumPy array or TensorFlow tensor in the shape (width, height) for grayscale.
        distorted_image: numpy.ndarray | tensorflow.Tensor | tensorflow.Variable
            The distorted image, which is to be compared to the reference image. The image can be in RGB or
            grayscale. The values must be in the range [0, 255]. The image must be a NumPy array or a
            TensorFlow tensor of the shape (width, height, 3) in the case of RGB, or a NumPy array or
            TensorFlow tensor in the shape (width, height) for grayscale.
        preprocess_with_subsampling: boolean
            An optional parameter, which determines whether a preprocessing step is to be performed, which
            accommodates for the viewing distance in psychophysical experiments.

    Returns:
    --------
        (float, numpy.ndarray | tensorflow.Tensor | tensorflow.Variable, numpy.ndarray | tensorflow.Tensor
        | tensorflow.Variable): Returns a three-tuple containing the similarity score, the similarity maps
        and the weight maps. The similarity score is the Haar wavelet-based perceptual similarity index,
        measured in the interval [0,1]. The similarity maps are maps of horizontal and vertical local
        similarities. For RGB images, this variable also includes a similarity map with respect to the two
        color channels in the YIQ space. The weight maps are maps that measure the importance of the local
        similarities in the similarity maps.
    """

    if is_numpy(reference_image) and is_numpy(distorted_image):
        return haar_psi_numpy(reference_image, distorted_image, preprocess_with_subsampling)
    elif is_tensorflow(reference_image) and is_tensorflow(distorted_image):
        if not is_tensorflow_available:
            raise ValueError(
                "TensorFlow is not installed. If you have TensorFlow installed, please check your installation.")
        return haar_psi_tensorflow(reference_image, distorted_image, preprocess_with_subsampling)
    else:
        raise ValueError(
            "The reference or the distorted image is neither a NumPy array, nor a TensorFlow tensor or variable. There are only NumPy and TensorFlow implementations available.")


def haar_psi_numpy(reference_image, distorted_image, preprocess_with_subsampling=True):
    """
    Calculates the HaarPSI perceptual similarity index between the two specified images. This implementation uses NumPy.

    Parameters:
    -----------
        reference_image: numpy.ndarray
            The reference image, which can be in RGB or grayscale. The values must be in the range [0, 255].
            The image must be a NumPy array of the shape (width, height, 3) in the case of RGB or a NumPy
            array in the shape (width, height) for grayscale.
        distorted_image: numpy.ndarray
            The distorted image, which is to be compared to the reference image. The image can be in RGB or
            grayscale. The values must be in the range [0, 255]. The image must be a NumPy array of the
            shape (width, height, 3) in the case of RGB or a NumPy array in the shape (width, height) for
            grayscale.
        preprocess_with_subsampling: boolean
            An optional parameter, which determines whether a preprocessing step is to be performed, which
            accommodates for the viewing distance in psychophysical experiments.

    Returns:
    --------
        (float, numpy.ndarray, numpy.ndarray): Returns a three-tuple containing the similarity score, the
        similarity maps and the weight maps. The similarity score is the Haar wavelet-based perceptual
        similarity index, measured in the interval [0,1]. The similarity maps are maps of horizontal and
        vertical local similarities. For RGB images, this variable also includes a similarity map with
        respect to the two color channels in the YIQ space. The weight maps are maps that measure the
        importance of the local similarities in the similarity maps.
    """

    # Checks if the image is a grayscale or an RGB image
    reference_image = numpy.int8(reference_image * 255)
    distorted_image = numpy.int8(distorted_image * 255)
    if reference_image.shape != distorted_image.shape:
        raise ValueError("The shapes of the reference image and the distorted image do not match.")
    if len(reference_image.shape) == 2:
        is_color_image = False
    elif reference_image.shape[2] == 1:
        is_color_image = False
    else:
        is_color_image = True

    # Converts the image values to double precision floating point numbers
    reference_image = reference_image.astype(numpy.float64)
    distorted_image = distorted_image.astype(numpy.float64)

    # The HaarPSI algorithm requires two constants, C and alpha, that have been experimentally determined
    # to be C = 30 and alpha = 4.2
    C = 30.0
    alpha = 4.2

    # If the images are in RGB, then they are transformed to the YIQ color space
    if is_color_image:
        reference_image_y = 0.299 * reference_image[:, :, 0] + 0.587 * reference_image[:, :,
                                                                       1] + 0.114 * reference_image[:, :, 2]
        distorted_image_y = 0.299 * distorted_image[:, :, 0] + 0.587 * distorted_image[:, :,
                                                                       1] + 0.114 * distorted_image[:, :, 2]
        reference_image_i = 0.596 * reference_image[:, :, 0] - 0.274 * reference_image[:, :,
                                                                       1] - 0.322 * reference_image[:, :, 2]
        distorted_image_i = 0.596 * distorted_image[:, :, 0] - 0.274 * distorted_image[:, :,
                                                                       1] - 0.322 * distorted_image[:, :, 2]
        reference_image_q = 0.211 * reference_image[:, :, 0] - 0.523 * reference_image[:, :,
                                                                       1] + 0.312 * reference_image[:, :, 2]
        distorted_image_q = 0.211 * distorted_image[:, :, 0] - 0.523 * distorted_image[:, :,
                                                                       1] + 0.312 * distorted_image[:, :, 2]
    else:
        reference_image_y = reference_image
        distorted_image_y = distorted_image

    # Subsamples the images, which simulates the typical distance between an image and its viewer
    if preprocess_with_subsampling:
        reference_image_y = subsample(reference_image_y)
        distorted_image_y = subsample(distorted_image_y)
        if is_color_image:
            reference_image_i = subsample(reference_image_i)
            distorted_image_i = subsample(distorted_image_i)
            reference_image_q = subsample(reference_image_q)
            distorted_image_q = subsample(distorted_image_q)

    # Performs the Haar wavelet decomposition
    number_of_scales = 3
    coefficients_reference_image_y = haar_wavelet_decompose(reference_image_y, number_of_scales)
    coefficients_distorted_image_y = haar_wavelet_decompose(distorted_image_y, number_of_scales)
    if is_color_image:
        coefficients_reference_image_i = numpy.abs(convolve2d(reference_image_i, numpy.ones((2, 2)) / 4.0, mode="same"))
        coefficients_distorted_image_i = numpy.abs(convolve2d(distorted_image_i, numpy.ones((2, 2)) / 4.0, mode="same"))
        coefficients_reference_image_q = numpy.abs(convolve2d(reference_image_q, numpy.ones((2, 2)) / 4.0, mode="same"))
        coefficients_distorted_image_q = numpy.abs(convolve2d(distorted_image_q, numpy.ones((2, 2)) / 4.0, mode="same"))

    # Pre-allocates the variables for the local similarities and the weights
    if is_color_image:
        local_similarities = numpy.zeros(sum([reference_image_y.shape, (3,)], ()))
        weights = numpy.zeros(sum([reference_image_y.shape, (3,)], ()))
    else:
        local_similarities = numpy.zeros(sum([reference_image_y.shape, (2,)], ()))
        weights = numpy.zeros(sum([reference_image_y.shape, (2,)], ()))

    # Computes the weights and similarities for each orientation
    for orientation in range(2):
        weights[:, :, orientation] = numpy.maximum(
            numpy.abs(coefficients_reference_image_y[:, :, 2 + orientation * number_of_scales]),
            numpy.abs(coefficients_distorted_image_y[:, :, 2 + orientation * number_of_scales])
        )
        coefficients_reference_image_y_magnitude = numpy.abs(
            coefficients_reference_image_y[:, :, (orientation * number_of_scales, 1 + orientation * number_of_scales)])
        coefficients_distorted_image_y_magnitude = numpy.abs(
            coefficients_distorted_image_y[:, :, (orientation * number_of_scales, 1 + orientation * number_of_scales)])
        local_similarities[:, :, orientation] = numpy.sum(
            (2 * coefficients_reference_image_y_magnitude * coefficients_distorted_image_y_magnitude + C) / (
                    coefficients_reference_image_y_magnitude ** 2 + coefficients_distorted_image_y_magnitude ** 2 + C),
            axis=2
        ) / 2

    # Computes the similarities for color channels
    if is_color_image:
        similarity_i = (2 * coefficients_reference_image_i * coefficients_distorted_image_i + C) / (
                coefficients_reference_image_i ** 2 + coefficients_distorted_image_i ** 2 + C)
        similarity_q = (2 * coefficients_reference_image_q * coefficients_distorted_image_q + C) / (
                coefficients_reference_image_q ** 2 + coefficients_distorted_image_q ** 2 + C)
        local_similarities[:, :, 2] = (similarity_i + similarity_q) / 2
        weights[:, :, 2] = (weights[:, :, 0] + weights[:, :, 1]) / 2

    # Calculates the final score
    similarity = logit(numpy.sum(sigmoid(local_similarities[:], alpha) * weights[:]) / numpy.sum(weights[:]),
                       alpha) ** 2

    # Returns the result
    return similarity, local_similarities, weights


# def haar_id_loss(x_pred, x_real, x_pred_logit, batch_size, id_ratio=0.3):
#     haarloss = haar_psi_loss(x_real, x_pred_logit, batch_size)
#     v6_net, idloss = face_id_loss(x_pred, x_real)
#     haaridloss = tf.multiply((1 - id_ratio), (1 - haarloss)) + tf.multiply(idloss, id_ratio)
#     return v6_net, haaridloss, idloss


def haar_psi_loss(x_real, x_pred, batch_size):
    x_real = tf.floor(((x_real + 1) / 2) * 255)
    x_pred = tf.floor(((x_pred + 1) / 2) * 255)
    total_haarpsi = 0
    for i in range(batch_size):
        total_haarpsi += haar_psi_tensorflow(x_real[i, :, :, :], x_pred[i, :, :, :])[0]
    mean_haarpsi = total_haarpsi / batch_size
    return 1 - mean_haarpsi


def haar_psi_tensorflow(reference_image, distorted_image, preprocess_with_subsampling=True):
    """
    Calculates the HaarPSI perceptual similarity index between the two specified images. This implementation uses TensorFlow.

    Parameters:
    -----------
        reference_image: tensorflow.Tensor | tensorflow.Variable
            The reference image, which can be in RGB or grayscale. The values must be in the range [0, 255].
            The image must be a TensorFlow Tensor of the shape (width, height, 3) in the case of RGB or a
            TensorFlow tensor in the shape (width, height) for grayscale.
        distorted_image: tensorflow.Tensor | tensorflow.Variable
            The distorted image, which is to be compared to the reference image. The image can be in RGB or
            grayscale. The values must be in the range [0, 255]. The image must be a TensorFlow tensor of
            the shape (width, height, 3) in the case of RGB or a TensorFlow tensor in the shape
            (width, height) for grayscale.
        preprocess_with_subsampling: boolean
            An optional parameter, which determines whether a preprocessing step is to be performed, which
            accommodates for the viewing distance in psychophysical experiments.

    Returns:
    --------
        (float, tensorflow.Tensor, tensorflow.Tensor): Returns a three-tuple containing the similarity score,
        the similarity maps and the weight maps. The similarity score is the Haar wavelet-based perceptual
        similarity index, measured in the interval [0,1]. The similarity maps are maps of horizontal and
        vertical local similarities. For RGB images, this variable also includes a similarity map with
        respect to the two color channels in the YIQ space. The weight maps are maps that measure the
        importance of the local similarities in the similarity maps.
    """

    if not is_tensorflow_available:
        raise ValueError(
            "TensorFlow is not installed. If you have TensorFlow installed, please check your installation.")

    # Checks if the images are both single precision floats
    if reference_image.dtype != tf.float32:
        raise ValueError("The reference image has to be single precision float.")
    if distorted_image.dtype != tf.float32:
        raise ValueError("The distorted image has to be single precision float.")

    # Checks if the image is a grayscale or an RGB image
    if reference_image.get_shape().as_list() != distorted_image.get_shape().as_list():
        raise ValueError("The shapes of the reference image and the distorted image do not match.")
    if len(reference_image.get_shape().as_list()) == 2:
        is_color_image = False
    elif reference_image.get_shape().as_list()[2] == 1:
        is_color_image = False
    else:
        is_color_image = True

    # The HaarPSI algorithm requires two constants, C and alpha, that have been experimentally determined
    # to be C = 30 and alpha = 4.2
    C = tf.constant(30.0, dtype=tf.float32)
    alpha = tf.constant(4.2, dtype=tf.float32)

    # If the images are in RGB, then they are transformed to the YIQ color space
    if is_color_image:
        reference_image_y = 0.299 * reference_image[:, :, 0] + 0.587 * reference_image[:, :,
                                                                       1] + 0.114 * reference_image[:, :, 2]
        distorted_image_y = 0.299 * distorted_image[:, :, 0] + 0.587 * distorted_image[:, :,
                                                                       1] + 0.114 * distorted_image[:, :, 2]
        reference_image_i = 0.596 * reference_image[:, :, 0] - 0.274 * reference_image[:, :,
                                                                       1] - 0.322 * reference_image[:, :, 2]
        distorted_image_i = 0.596 * distorted_image[:, :, 0] - 0.274 * distorted_image[:, :,
                                                                       1] - 0.322 * distorted_image[:, :, 2]
        reference_image_q = 0.211 * reference_image[:, :, 0] - 0.523 * reference_image[:, :,
                                                                       1] + 0.312 * reference_image[:, :, 2]
        distorted_image_q = 0.211 * distorted_image[:, :, 0] - 0.523 * distorted_image[:, :,
                                                                       1] + 0.312 * distorted_image[:, :, 2]
    else:
        reference_image_y = reference_image
        distorted_image_y = distorted_image

    # Subsamples the images, which simulates the typical distance between an image and its viewer
    if preprocess_with_subsampling:
        reference_image_y = subsample(reference_image_y)
        distorted_image_y = subsample(distorted_image_y)
        if is_color_image:
            reference_image_i = subsample(reference_image_i)
            distorted_image_i = subsample(distorted_image_i)
            reference_image_q = subsample(reference_image_q)
            distorted_image_q = subsample(distorted_image_q)

    # Performs the Haar wavelet decomposition
    number_of_scales = 3
    coefficients_reference_image_y = haar_wavelet_decompose(reference_image_y, number_of_scales)
    coefficients_distorted_image_y = haar_wavelet_decompose(distorted_image_y, number_of_scales)
    if is_color_image:
        coefficients_reference_image_i = tf.abs(convolve2d(reference_image_i, tf.ones((2, 2)) / 4.0, mode="same"))
        coefficients_distorted_image_i = tf.abs(convolve2d(distorted_image_i, tf.ones((2, 2)) / 4.0, mode="same"))
        coefficients_reference_image_q = tf.abs(convolve2d(reference_image_q, tf.ones((2, 2)) / 4.0, mode="same"))
        coefficients_distorted_image_q = tf.abs(convolve2d(distorted_image_q, tf.ones((2, 2)) / 4.0, mode="same"))

    # Pre-allocates the variables for the local similarities and the weights
    if is_color_image:
        local_similarities = [tf.zeros_like(reference_image_y)] * 3
        weights = [tf.zeros_like(reference_image_y)] * 3
    else:
        local_similarities = [tf.zeros_like(reference_image_y)] * 2
        weights = [tf.zeros_like(reference_image_y)] * 2

    # Computes the weights and similarities for each orientation
    for orientation in range(2):
        weights[orientation] = tf.maximum(
            tf.abs(coefficients_reference_image_y[:, :, 2 + orientation * number_of_scales]),
            tf.abs(coefficients_distorted_image_y[:, :, 2 + orientation * number_of_scales])
        )
        coefficients_reference_image_y_magnitude = tf.abs(
            coefficients_reference_image_y[:, :, orientation * number_of_scales:2 + orientation * number_of_scales])
        coefficients_distorted_image_y_magnitude = tf.abs(
            coefficients_distorted_image_y[:, :, orientation * number_of_scales:2 + orientation * number_of_scales])
        local_similarities[orientation] = tf.reduce_sum(
            (2 * coefficients_reference_image_y_magnitude * coefficients_distorted_image_y_magnitude + C) / (
                    coefficients_reference_image_y_magnitude ** 2 + coefficients_distorted_image_y_magnitude ** 2 + C),
            axis=2
        ) / 2
    weights = tf.stack(weights, axis=-1)
    local_similarities = tf.stack(local_similarities, axis=-1)

    # Computes the similarities for color channels
    if is_color_image:
        similarity_i = (2 * coefficients_reference_image_i * coefficients_distorted_image_i + C) / (
                coefficients_reference_image_i ** 2 + coefficients_distorted_image_i ** 2 + C)
        similarity_q = (2 * coefficients_reference_image_q * coefficients_distorted_image_q + C) / (
                coefficients_reference_image_q ** 2 + coefficients_distorted_image_q ** 2 + C)
        local_similarities = tf.concat(
            [local_similarities[:, :, slice(0, 2)], tf.expand_dims((similarity_i + similarity_q) / 2, axis=2)], axis=2)
        weights = tf.concat(
            [weights[:, :, slice(0, 2)], tf.expand_dims((weights[:, :, 0] + weights[:, :, 1]) / 2, axis=2)], axis=2)

    # Calculates the final score
    similarity = logit(tf.reduce_sum(sigmoid(local_similarities[:], alpha) * weights[:]) / tf.reduce_sum(weights[:]),
                       alpha) ** 2

    # Returns the result
    return similarity, local_similarities, weights


def subsample(image):
    """
    Convolves the specified image with a 2x2 mean filter and performs a dyadic subsampling step. This
    simulates the typical distance between an image and its viewer.

    Parameters:
    -----------
        image: numpy.ndarray | tensorflow.Tensor | tensorflow.Variable
            The image that is to be subsampled.

    Returns:
    --------
        numpy.ndarray | tensorflow.Tensor: Returns the subsampled image.
    """

    if is_numpy(image):
        subsampled_image = convolve2d(image, numpy.ones((2, 2)) / 4.0, mode="same")
    elif is_tensorflow(image):
        if not is_tensorflow_available:
            raise ValueError(
                "TensorFlow is not installed. If you have TensorFlow installed, please check your installation.")
        subsampled_image = convolve2d(image, tf.ones((2, 2)) / 4.0, mode="same")
    else:
        raise ValueError(
            "The image is neither a NumPy array, nor a TensorFlow tensor or variable. There are only NumPy and TensorFlow implementations available.")

    subsampled_image = subsampled_image[::2, ::2]
    return subsampled_image


def convolve2d(data, kernel, mode="same"):
    """
    Convolves the first input array with the second one in the same way MATLAB does. Due to an
    implementation detail, the SciPy and MATLAB implementations yield different results. This method
    rectifies this shortcoming of the SciPy implementation.

    Parameters:
    -----------
        data: numpy.ndarray | tensorflow.Tensor | tensorflow.Variable
            The first input array.
        kernel: numpy.ndarray | tensorflow.Tensor | tensorflow.Variable
            The second input array with which the fist input array is being convolved.
        mode: str
            A string indicating the size of the output.

    Returns:
    --------
        numpy.ndarray | tensorflow.Tensor: Returns a 2-dimensional array containing a subset of the discrete
        linear convolution of the first input array with the second input array.
    """

    # Checks if the NumPy or the TensorFlow implementation is to be used
    if is_numpy(data) and is_numpy(kernel):

        # Due to an implementation detail of MATLAB, the input arrays have to be rotated by 90 degrees to
        # retrieve a similar result as compared to MATLAB
        rotated_data = numpy.rot90(data, 2)
        rotated_kernel = numpy.rot90(kernel, 2)

        # The convolution result has to be rotated again by 90 degrees to get the same result as in MATLAB
        result = signal.convolve2d(
            rotated_data,
            rotated_kernel,
            mode=mode
        )
        result = numpy.rot90(result, 2)

    elif is_tensorflow(data) and is_tensorflow(kernel):

        if not is_tensorflow_available:
            raise ValueError(
                "TensorFlow is not installed. If you have TensorFlow installed, please check your installation.")

        # TensorFlow requires a 4D Tensor for convolution, the data has to be shaped [batch_size, width, height, number_of_channels]
        # and the kernel has to be shaped [width, height, number_of_channels_in, number_of_channels_out]
        data_shape = data.get_shape().as_list()
        data = tf.reshape(data, [1, data_shape[0], data_shape[1], 1])
        kernel_shape = kernel.get_shape().as_list()
        kernel = tf.reshape(kernel, [kernel_shape[0], kernel_shape[1], 1, 1])

        # Calculates the convolution, for some reason that I do not fully understand, the result has to be negated
        result = tf.nn.conv2d(
            data,
            kernel,
            padding=mode.upper(),
            strides=[1, 1, 1, 1]
        )
        result = tf.negative(tf.squeeze(result))

    else:
        raise ValueError(
            "Either the data or the kernel is neither a NumPy array, nor a TensorFlow tensor or variable. There are only NumPy and TensorFlow implementations available.")

    # Returns the result of the convolution
    return result


def haar_wavelet_decompose(image, number_of_scales):
    """
    Performs the Haar wavelet decomposition.

    Parameters:
    -----------
        image: numpy.ndarray | tensorflow.Tensor | tensorflow.Variable
            The image that is to be decomposed.
        number_of_scales: int
            The number different filter scales that is to be used.

    Returns:
    --------
        numpy.ndarray | tensorflow.Tensor: Returns the coefficients that were determined by the Haar wavelet
        decomposition.
    """

    if is_numpy(image):

        coefficients = numpy.zeros(sum([image.shape, (2 * number_of_scales,)], ()))
        for scale in range(1, number_of_scales + 1):
            haar_filter = 2 ** (-scale) * numpy.ones((2 ** scale, 2 ** scale))
            haar_filter[:haar_filter.shape[0] // 2, :] = -haar_filter[:haar_filter.shape[0] // 2, :]
            coefficients[:, :, scale - 1] = convolve2d(image, haar_filter, mode="same")
            coefficients[:, :, scale + number_of_scales - 1] = convolve2d(image, numpy.transpose(haar_filter),
                                                                          mode="same")

    elif is_tensorflow(image):

        if not is_tensorflow_available:
            raise ValueError(
                "TensorFlow is not installed. If you have TensorFlow installed, please check your installation.")

        coefficients = [None] * (2 * number_of_scales)
        for scale in range(1, number_of_scales + 1):
            upper_part = -2 ** (-scale) * tf.ones((2 ** scale // 2, 2 ** scale))
            lower_part = 2 ** (-scale) * tf.ones((2 ** scale // 2, 2 ** scale))
            haar_filter = tf.concat([upper_part, lower_part], axis=0)
            coefficients[scale - 1] = convolve2d(image, haar_filter, mode="same")
            coefficients[scale + number_of_scales - 1] = convolve2d(image, tf.transpose(haar_filter), mode="same")
        coefficients = tf.stack(coefficients, axis=-1)

    else:
        raise ValueError(
            "The image is neither a NumPy array, nor a TensorFlow tensor or variable. There are only NumPy and TensorFlow implementations available.")

    return coefficients


def sigmoid(value, alpha):
    """
    Applies the sigmoid (logistic) function to the specified value.

    Parameters:
    -----------
        value: int | float | numpy.ndarray | tensorflow.Tensor | tensorflow.Variable
            The value to which the sigmoid function is to be applied.
        alpha: float
            The steepness of the "S"-shaped curve produced by the sigmoid function.

    Returns:
    --------
        int | float | numpy.ndarray | tensorflow.Tensor: Returns the result of the sigmoid function.
    """

    if is_numpy(value):
        return 1.0 / (1.0 + numpy.exp(-alpha * value))
    elif is_tensorflow(value):
        if not is_tensorflow_available:
            raise ValueError(
                "TensorFlow is not installed. If you have TensorFlow installed, please check your installation.")
        return 1.0 / (1.0 + tf.exp(-alpha * value))
    else:
        raise ValueError(
            "The value is neither a NumPy array, nor a TensorFlow tensor or variable. There are only NumPy and TensorFlow implementations available.")


def logit(value, alpha):
    """
    Applies the logit function to the specified value, which is the reverse of the sigmoid
    (logistic) function.

    Parameters:
    -----------
        value: int | float | numpy.ndarray | tensorflow.Tensor | tensorflow.Variable
            The value to which the logit function is to be applied.
        alpha: float
            The steepness of the "S"-shaped curve produced by the logit function.

    Returns:
    --------
        int | float | tensorflow.Tensor: Returns the result of the logit function.
    """

    if is_numpy(value):
        return numpy.log(value / (1 - value)) / alpha
    elif is_tensorflow(value):
        if not is_tensorflow_available:
            raise ValueError(
                "TensorFlow is not installed. If you have TensorFlow installed, please check your installation.")
        return tf.log(value / (1 - value)) / alpha
    else:
        raise ValueError(
            "The value is neither a NumPy array, nor a TensorFlow tensor or variable. There are only NumPy and TensorFlow implementations available.")


def is_numpy(value):
    """
    Determines whether the specified value is a NumPy value, i.e. an numpy.ndarray or a NumPy scalar, etc.

    Parameters:
    -----------
        value:
            The value for which is to be determined if it is a NumPy value or not.

    Returns:
    --------
        boolean: Returns True if the value is a NumPy value and False otherwise.
    """

    return type(value).__module__.split(".")[0] == "numpy"


def is_tensorflow(value):
    """
    Determines whether the specified value is a TensorFlow value, i.e. an tensorflow.Variable or a
    tensorflow.Tensor, etc.

    Parameters:
    -----------
        value:
            The value for which is to be determined if it is a TensorFlow value or not.

    Returns:
    --------
        boolean: Returns True if the value is a TensorFlow value and False otherwise.
    """

    if not is_tensorflow_available:
        raise ValueError(
            "TensorFlow is not installed. If you have TensorFlow installed, please check your installation.")

    return type(value).__module__.split(".")[0] == "tensorflow"
