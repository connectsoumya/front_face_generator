import tensorflow as tf
import sys
from os.path import dirname, abspath
import logging.config
import logging
from configparser import ConfigParser
from configparser import NoSectionError

parser = ConfigParser()
d = dirname(dirname(dirname(abspath(__file__))))

try:
    parser.read('../config.ini')
    logging.config.fileConfig('../logging.ini')
except Exception as e:
    parser.read('config.ini')
    logging.config.fileConfig('logging.ini')
bias = False


def dense(input_, units, bias=True, name=None):
    return tf.layers.Dense(units=units, use_bias=bias, name=name)(input_)


def conv2d(input_, filters, kernel_size, strides, padding='SAME', bias=bias, name=None):
    assert name is not None
    ip_channel = input_.get_shape().as_list()[-1]
    dims = [kernel_size, kernel_size, ip_channel, filters]
    #
    filter_ = tf.get_variable(name + '_filter', dims, dtype=tf.float32,
                             initializer=tf.random_normal_initializer(0, 0.02))
                             #tf.contrib.layers.variance_scaling_initializer(factor=2.0, mode='FAN_IN',
                                                                                        # uniform=False, seed=None,
                                                                                        # dtype=tf.float32))
    strides = [1, strides, strides, 1]
    conv = tf.nn.conv2d(input_, filter_, strides=strides, padding=padding, name=name)
    if bias:
        b_init = tf.random_normal_initializer(0, 0.002)
        b = tf.get_variable('b', shape=(filters,), initializer=b_init)
        conv = tf.nn.bias_add(conv, b)
    return conv


def conv2d_spectral(input_, filters, strides, padding, bias=bias, name=None):
    strides = [1, strides[0], strides[1], 1]
    conv = tf.nn.conv2d(input_, filters, strides=strides, padding=padding, name=name)
    if bias:
        b_init = tf.random_normal_initializer(0, 0.002)
        b = tf.get_variable('b', shape=(filters,), initializer=b_init)
        conv = tf.nn.bias_add(conv, b)
    return conv


def conv2d_transpose(input_, filters, kernel_size, strides, padding='SAME', bias=bias, name=None):
    conv = tf.keras.layers.Conv2DTranspose(filters, kernel_size, strides, padding=padding)(input_)
    if bias:
        b_init = tf.random_normal_initializer(0, 0.002)
        b = tf.get_variable('b', shape=(filters,), initializer=b_init)
        conv = tf.nn.bias_add(conv, b)
    return conv


def conv2d_transpose_pl(input_, filters, kernel_size, strides, padding, bias=bias, name=None):
    ip_shape = input_.get_shape().as_list()
    ip_channel = ip_shape[-1]
    # ip_shape_1 = tf.placeholder(dtype=tf.int32)
    # ip_shape_2 = tf.placeholder(dtype=tf.int32)
    dims = [kernel_size[0], kernel_size[1], filters, ip_channel]  # note the difference with 'dims' of conv2d
    filter_ = tf.get_variable('filter', dims, dtype=tf.float32, initializer=tf.random_normal_initializer(0, 0.02))
    strides = [1, strides[0], strides[1], 1]
    output_shape = [ip_shape[0], tf.multiply(ip_shape, 2), tf.multiply(ip_shape, 2), filters]
    conv = tf.nn.conv2d_transpose(input_, filter_, output_shape, strides, padding, name=name)
    if bias:
        b_init = tf.random_normal_initializer(0, 0.002)
        b = tf.get_variable('b', shape=(filters,), initializer=b_init)
        conv = tf.nn.bias_add(conv, b)
    return conv


def lrelu(input_, alpha=0.2, name=None):
    # adding these together creates the leak part and linear part
    # then cancels them out by subtracting/adding an absolute value term
    # leak: alpha*input/2 - alpha*abs(input)/2
    # linear: input/2 + abs(input)/2
    # this block looks like it has 2 inputs on the graph unless we do this
    # taken from https://github.com/affinelayer/pix2pix-tensorflow/blob/master/pix2pix.py
    name = name or 'lrelu'
    input_ = tf.identity(input_)
    with tf.name_scope(name):
        op = (0.5 * (1 + alpha)) * input_ + (0.5 * (1 - alpha)) * tf.abs(input_)
    return op


def batchnorm(input_, trainable=True, name=None):
    return tf.keras.layers.BatchNormalization(trainable=trainable, name=name)(input_)
    # mean = tf.constant(value=0.0, dtype=tf.fl)
    # return tf.nn.batch_normalization(input_, mean=0, variance=0.1, offset=None, scale=None, variance_epsilon=0.0001, name=name)


def spectral_norm(w, name='1', iteration=1):
    """
    Taken from  https://github.com/connectsoumya/Spectral_Normalization-Tensorflow
    Usage:
    w = tf.get_variable("kernel", shape=[kernel, kernel, x.get_shape()[-1], channels])
    b = tf.get_variable("bias", [channels], initializer=tf.constant_initializer(0.0))
    x = tf.nn.conv2d(input=x, filter=spectral_norm(w), strides=[1, stride, stride, 1]) + b
    :param w:
    :param iteration:
    :return:
    """
    w_shape = w.shape.as_list()
    w = tf.reshape(w, [-1, w_shape[-1]])
    u = tf.get_variable("u/" + name, [1, w_shape[-1]], initializer=tf.random_normal_initializer(), trainable=False)
    u_hat = u
    v_hat = None
    for i in range(iteration):
        """
        power iteration
        Usually iteration = 1 will be enough
        """
        v_ = tf.matmul(u_hat, tf.transpose(w))
        v_hat = tf.nn.l2_normalize(v_)

        u_ = tf.matmul(v_hat, w)
        u_hat = tf.nn.l2_normalize(u_)

    u_hat = tf.stop_gradient(u_hat)
    v_hat = tf.stop_gradient(v_hat)

    sigma = tf.matmul(tf.matmul(v_hat, w), tf.transpose(u_hat))

    with tf.control_dependencies([u.assign(u_hat)]):
        w_norm = w / sigma
        w_norm = tf.reshape(w_norm, [-1,] + w_shape[1:])

    return w_norm


def max_pool(input_, kernel_size, strides, padding='SAME', name=None):
    kernel = [1, kernel_size, kernel_size, 1]
    stride = [1, strides, strides, 1]
    return tf.nn.max_pool(input_, kernel, stride, padding=padding, name=name)


def avg_pooling(input_, kernel_size, strides, padding, name=None):
    kernel = [1, kernel_size[0], kernel_size[1], 1]
    stride = [1, strides[0], strides[1], 1]
    return tf.nn.avg_pool(input_, kernel, stride, padding=padding, name=name)


def maxout_unit(input_, filters, kernel_size, strides, padding, bias=True, name=None):
    # one unit of the maxout network
    name = name or 'maxout_unit'
    with tf.variable_scope(name):
        conv = conv2d(input_, filters, kernel_size, strides, padding, bias=bias)
        slice_1, slice_2 = conv[:, :, :, :filters / 2], conv[:, :, :, filters / 2:]
        return tf.maximum(slice_1, slice_2, name=name)


def prelu(input_, name=None):
    alphas = tf.get_variable('alpha', input_.get_shape()[-1], initializer=tf.constant_initializer(0.0))
    with tf.name_scope(name):
        pos = tf.nn.relu(input_)
        neg = alphas * (input_ - abs(input_)) * 0.5
    return pos + neg


def relu(input_):
    return tf.nn.relu(input_)


def pixelshuffle(input_, r):
    """
    Main OP that you can arbitrarily use in you tensorflow code for super resolution
    :param input_:
    :param r: upscale factor
    :return:
    """
    return tf.depth_to_space(input_, r)

def upsampling_nn(_input, factor=2):
    try:
        op = tf.keras.layers.UpSampling2D(factor, interpolation='nearest')(_input)
    except TypeError:
        op = tf.keras.layers.UpSampling2D(factor)(_input)

    return op
