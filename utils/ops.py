# coding: utf-8
import tensorflow as tf
from configparser import ConfigParser
import numpy as np
import logging
import logging.config

parser = ConfigParser()
try:
    parser.read('config.ini')
    logging.config.fileConfig('logging.ini')
except:
    parser.read('../config.ini')
    logging.config.fileConfig('../logging.ini')


def sampler(mean, log_stddev, latent_size):
    # we sample from the standard normal a matrix of batch_size * latent_size (taking into account minibatches)
    std_norm = tf.random_normal(shape=(tf.shape(mean)[0], latent_size), mean=0, stddev=1)
    # sampling from Z~N(μ, σ^2) is the same as sampling from μ + σX, X~N(0,1)
    return mean + tf.exp(log_stddev) * std_norm


def average_gradients(tower_grads):
    """Calculate the average gradient for each shared variable across all towers.
    Note that this function provides a synchronization point across all towers.
    Args:
      tower_grads: List of lists of (gradient, variable) tuples. The outer list
        is over individual gradients. The inner list is over the gradient
        calculation for each tower.
    Returns:
       List of pairs of (gradient, variable) where the gradient has been averaged
       across all towers.
    """
    average_grads = []
    for grad_and_vars in zip(*tower_grads):
        # Note that each grad_and_vars looks like the following:
        #   ((grad0_gpu0, var0_gpu0), ... , (grad0_gpuN, var0_gpuN))
        grads = []
        for g, _ in grad_and_vars:
            # Add 0 dimension to the gradients to represent the tower.
            try:
                expanded_g = tf.expand_dims(g, 0)
                # Append on a 'tower' dimension which we will average over below.
                grads.append(expanded_g)
                # Average over the 'tower' dimension.
                grad = tf.concat(axis=0, values=grads)
                grad = tf.reduce_mean(grad, 0)
                # Keep in mind that the Variables are redundant because they are shared across towers. So we will just return
                # the first tower's pointer to the Variable.
                v = grad_and_vars[0][1]
                grad_and_var = (grad, v)
                average_grads.append(grad_and_var)
            except:
                pass
    return average_grads


def sigmoid(x):
    if type(x) is list:
        op = [sigmoid(arr) for arr in x]
        return op
    else:
        return 1 / (1 + np.exp(-x))


def save(sess, filepath='../tmp/tfmodel.mdl', global_step=None):
    """
    Save a TensorFlow model.
    :param sess:
    :param filepath:
    :param global_step:
    :return:
    """
    saver = tf.train.Saver()
    saver.save(sess, filepath, global_step=global_step)
    logging.info('Model saved at ' + filepath)


def load(sess, filepath, var=None):
    """
    Load/Restore a TensorFlow model.
    :param sess: The session
    :param filepath:
    :return:
    """
    init_op = tf.global_variables_initializer()
    sess.run(init_op)
    if var is None:
        saver = tf.train.Saver()
    else:
        saver = tf.train.Saver(var)
    saver.restore(sess, filepath)
    logging.info('Model restored from ' + filepath)
    return sess


class Adam(tf.train.AdamOptimizer):
    def __init__(self, learning_rate, ):
        super(Adam, self).__init__(learning_rate=learning_rate)

    def compute_and_clip_gradients(self, loss, var_list):
        grads_and_vars = self.compute_gradients(loss, var_list)
        capped_gvs = [(tf.clip_by_value(grad, -1., 1.), var) for grad, var in grads_and_vars if grad is not None]
        return capped_gvs
