import tensorflow as tf
import os
from os.path import join
from os.path import abspath
import numpy as np
import glob
from itertools import chain
import random
import copy


def _parse_function(filename, label):
    image_string = tf.read_file(filename)
    image = tf.image.decode_jpeg(image_string, channels=3)
    image = tf.image.convert_image_dtype(image, tf.float32)
    # image = tf.subtract(tf.multiply(image, 2), 1)
    # Resize image if required
    # image = tf.image.resize_images(image, [64, 64])
    # label = tf.strings.to_number(label, out_type=tf.int32)
    return image, label


def _parse_function_with_parsemaps(filename, parsemaps, label):
    image_string = tf.read_file(filename)
    image = tf.image.decode_jpeg(image_string, channels=3)
    image = tf.image.convert_image_dtype(image, tf.float32)
    image = tf.image.resize_images(image, [128, 128])
    maps = []
    for pmap_file in tf.unstack(parsemaps):
        parsemaps_string = tf.read_file(pmap_file)
        pmaps = tf.image.decode_jpeg(parsemaps_string, channels=1)
        pmaps = tf.image.convert_image_dtype(pmaps, tf.float32)
        maps.append(pmaps)
    maps = tf.concat(maps, -1)
    label_string = tf.read_file(label)
    label = tf.image.decode_jpeg(label_string, channels=3)
    label = tf.image.convert_image_dtype(label, tf.float32)
    # label = tf.image.resize_images(label, [128, 128])
    # image = tf.subtract(tf.multiply(image, 2), 1)
    # Resize image if required
    # image = tf.image.resize_images(image, [64, 64])
    # label = tf.strings.to_number(label, out_type=tf.int32)
    return image, maps, label


def _parse_function_with_bwparsemap(filename, k, label):
    image_string = tf.read_file(filename)
    image = tf.image.decode_jpeg(image_string, channels=3)
    image = tf.image.convert_image_dtype(image, tf.float32)
    image = tf.image.resize_images(image, [128, 128])
    maps = tf.image.rgb_to_grayscale(image)
    label_string = tf.read_file(label)
    label = tf.image.decode_jpeg(label_string, channels=3)
    label = tf.image.convert_image_dtype(label, tf.float32)
    # label = tf.image.resize_images(label, [128, 128])
    # image = tf.subtract(tf.multiply(image, 2), 1)
    # Resize image if required
    # image = tf.image.resize_images(image, [64, 64])
    # label = tf.strings.to_number(label, out_type=tf.int32)
    return image, maps, label


def _parse_function_renren(degraded, original, gt, g):
    image_string = tf.read_file(degraded)
    deg_image = tf.image.decode_jpeg(image_string, channels=3)
    deg_image = tf.image.convert_image_dtype(deg_image, tf.float32)
    deg_image = tf.image.resize_images(deg_image, [128, 128])

    image_string = tf.read_file(original)
    ori_image = tf.image.decode_jpeg(image_string, channels=3)
    ori_image = tf.image.convert_image_dtype(ori_image, tf.float32)
    ori_image = tf.image.resize_images(ori_image, [128, 128])

    image_string = tf.read_file(gt)
    gt_image = tf.image.decode_jpeg(image_string, channels=3)
    gt_image = tf.image.convert_image_dtype(gt_image, tf.float32)
    gt_image = tf.image.resize_images(gt_image, [128, 128])

    # label = tf.image.resize_images(label, [128, 128])
    # image = tf.subtract(tf.multiply(image, 2), 1)
    # Resize image if required
    # image = tf.image.resize_images(image, [64, 64])
    # label = tf.strings.to_number(label, out_type=tf.int32)
    return deg_image, ori_image, gt_image, g


def _train_preprocess(image, label, use_random_flip=False):
    if use_random_flip:
        image = tf.image.random_flip_left_right(image)
    image = tf.image.random_brightness(image, max_delta=32.0 / 255.0)
    image = tf.image.random_saturation(image, lower=0.5, upper=1.5)
    # Make sure the image is still in [0, 1]
    image = tf.clip_by_value(image, 0.0, 1.0)
    return image, label


def get_all_files_and_class_multipie(root_path):
    data = []
    class_ = []
    for path, subdirs, files in os.walk(root_path):
        for name in files:
            class_.append(name[:3])
            data.append(os.path.join(path, name))
    np.save('multipie_gt_class', [data, class_])


# def get_all_files_helen(root_path):
#     data = []
#     for path, subdirs, files in os.walk(root_path):
#         for name in files:
#             class_.append(name[:3])
#             data.append(os.path.join(path, name))
#     np.save('helen_data', [data, class_])


def data_generator(filenames, labels, params, is_training=True):
    """
    The actual data generation process.
    Args:
        filenames: (list) filenames of the images.
        labels: (list) corresponding list of labels.
        params: (Params) contains hyperparameters of the model (ex: `params.num_epochs`).
        is_training: (bool) whether to use the train or test pipeline.

    Returns:
        inputs: (dict) A dictionary containing the batch data.
    """
    num_samples = len(filenames)
    assert len(filenames) == len(labels)
    parse_fn = lambda f, l: _parse_function(f, l)
    # train_fn = lambda f, l: train_preprocess(f, l)  # uncomment for preprocessing
    with tf.device('/cpu:0'):
        if is_training:
            dataset = (tf.data.Dataset.from_tensor_slices((tf.constant(filenames), tf.constant(labels)))
                       .shuffle(num_samples)  # whole dataset into the buffer ensures good shuffling
                       .map(parse_fn, num_parallel_calls=params.num_parallel_calls)
                       # .map(train_fn, num_parallel_calls=params.num_parallel_calls)  # uncomment for preprocessing
                       .batch(params.batch_size)
                       .prefetch(1)  # make sure you always have one batch ready to serve
                       )
        else:
            dataset = (tf.data.Dataset.from_tensor_slices((tf.constant(filenames), tf.constant(labels)))
                       .map(parse_fn)
                       .batch(params.batch_size)
                       .prefetch(1)  # make sure you always have one batch ready to serve
                       )
        # Create reinitializable iterator from dataset
        iterator = dataset.make_initializable_iterator()
    return iterator


def data_generator_parsemaps(filenames, params, is_training=True):
    """
    The actual data generation process.
    Args:
        filenames: (list) filenames of the images.
        labels: (list) corresponding list of labels.
        params: (Params) contains hyperparameters of the model (ex: `params.num_epochs`).
        is_training: (bool) whether to use the train or test pipeline.

    Returns:
        inputs: (dict) A dictionary containing the batch data.
    """
    num_samples = len(filenames)
    labels = [fname.replace('labels', 'images') + '.jpg' for fname in filenames]
    parsemaps = [sorted(glob.glob(fname + '/*.png')) for fname in filenames]
    data = [fname.replace('resized_crop', 'resized_crop_degraded') for fname in labels]
    parse_fn = lambda f, p, l: _parse_function_with_parsemaps(f, p, l)
    # train_fn = lambda f, l: train_preprocess(f, l)  # uncomment for preprocessing
    with tf.device('/cpu:0'):
        if is_training:
            dataset = (tf.data.Dataset.from_tensor_slices(
                (tf.constant(data), tf.constant(parsemaps), tf.constant(labels)))
                       .shuffle(num_samples)  # whole dataset into the buffer ensures good shuffling
                       .map(parse_fn, num_parallel_calls=params.num_parallel_calls)
                       # .map(train_fn, num_parallel_calls=params.num_parallel_calls)  # uncomment for preprocessing
                       .batch(params.batch_size)
                       .prefetch(1)  # make sure you always have one batch ready to serve
                       )
        else:
            dataset = (tf.data.Dataset.from_tensor_slices((tf.constant(data), tf.constant(labels)))
                       .map(parse_fn)
                       .batch(params.batch_size)
                       .prefetch(1)  # make sure you always have one batch ready to serve
                       )
        # Create reinitializable iterator from dataset
        iterator = dataset.make_initializable_iterator()
    return iterator


def data_generator_bwmap(filenames, params, is_training=True):
    """
    The actual data generation process.
    Args:
        filenames: (list) filenames of the images.
        labels: (list) corresponding list of labels.
        params: (Params) contains hyperparameters of the model (ex: `params.num_epochs`).
        is_training: (bool) whether to use the train or test pipeline.

    Returns:
        inputs: (dict) A dictionary containing the batch data.
    """
    num_samples = len(filenames)
    labels = [fname.replace('labels', 'images') + '.jpg' for fname in filenames]
    data = [fname.replace('resized_crop', 'resized_crop_degraded') for fname in labels]
    parse_fn = lambda f, p, l: _parse_function_with_bwparsemap(f, p, l)
    # train_fn = lambda f, l: train_preprocess(f, l)  # uncomment for preprocessing
    with tf.device('/cpu:0'):
        if is_training:
            dataset = (tf.data.Dataset.from_tensor_slices(
                (tf.constant(data), tf.constant(labels), tf.constant(labels)))
                       .shuffle(num_samples)  # whole dataset into the buffer ensures good shuffling
                       .map(parse_fn, num_parallel_calls=params.num_parallel_calls)
                       # .map(train_fn, num_parallel_calls=params.num_parallel_calls)  # uncomment for preprocessing
                       .batch(params.batch_size)
                       .prefetch(1)  # make sure you always have one batch ready to serve
                       )
        else:
            dataset = (tf.data.Dataset.from_tensor_slices((tf.constant(data), tf.constant(labels)))
                       .map(parse_fn)
                       .batch(params.batch_size)
                       .prefetch(1)  # make sure you always have one batch ready to serve
                       )
        # Create reinitializable iterator from dataset
        iterator = dataset.make_initializable_iterator()
    return iterator


def data_generator_renren(deg_folder, params, is_training=True):
    """
    The actual data generation process.
    Args:
        deg_folder: (list) filenames of the images.
        labels: (list) corresponding list of labels.
        params: (Params) contains hyperparameters of the model (ex: `params.num_epochs`).
        is_training: (bool) whether to use the train or test pipeline.

    Returns:
        inputs: (dict) A dictionary containing the batch data.
    """
    num_samples = len(deg_folder)
    degraded_fnames = list(chain.from_iterable([glob.glob(folder + '/*.jpg') for folder in deg_folder]))
    random.shuffle(degraded_fnames)
    original_fnames = [f.replace('DEGRADED_mixed', '') for f in degraded_fnames]
    gt_fnames = [join(abspath(join(f.replace('DEGRADED_mixed', 'GT'), os.pardir)), 'gt.jpg') for f in degraded_fnames]
    # gt_fnames_neg = copy.deepcopy(gt_fnames)
    # gt_fnames_neg.insert(0, gt_fnames_neg.pop(-1))
    parse_fn = lambda d, o, g, fn: _parse_function_renren(d, o, g, fn)
    # train_fn = lambda f, l: train_preprocess(f, l)  # uncomment for preprocessing
    with tf.device('/cpu:0'):
        if is_training:
            dataset = (tf.data.Dataset.from_tensor_slices(
                (tf.constant(degraded_fnames), tf.constant(original_fnames), tf.constant(gt_fnames),
                 tf.constant(gt_fnames)))
                       .shuffle(num_samples)  # whole dataset into the buffer ensures good shuffling
                       .map(parse_fn, num_parallel_calls=params.num_parallel_calls)
                       # .map(train_fn, num_parallel_calls=params.num_parallel_calls)  # uncomment for preprocessing
                       .batch(params.batch_size)
                       .prefetch(1)  # make sure you always have one batch ready to serve
                       )
        else:
            dataset = (tf.data.Dataset.from_tensor_slices(
                (tf.constant(degraded_fnames), tf.constant(original_fnames), tf.constant(gt_fnames)))
                       .map(parse_fn)
                       .batch(params.batch_size)
                       .prefetch(1)  # make sure you always have one batch ready to serve
                       )
        # Create reinitializable iterator from dataset
        iterator = dataset.make_initializable_iterator()
    return iterator


if __name__ == '__main__':
    # ('/media/soumya/HDD_2/MultiPIE_crop/ground_truth')
    pass
