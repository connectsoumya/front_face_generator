from __future__ import print_function
import matplotlib.pyplot as plt
import logging.config
import numpy as np
import cv2
from cv2 import imwrite
import skvideo.io as sio
from multiprocessing import Pool
import os
import glob
import errno
import logging
import logging.config

try:
    logging.config.fileConfig('../logging.ini')
    if not os.path.exists('../logger.log'):
        f = open('../logger.log', 'w+')
        f.close()
except Exception as e:
    logging.config.fileConfig('logging.ini')
    if not os.path.exists('logger.log'):
        f = open('logger.log', 'w+')
        f.close()
image_load_path = '/media/soumya/HDD_2/Helen/resized_crop/images'


def getListOfFiles(dirName):
    # create a list of file and sub directories
    # names in the given directory
    listOfFile = os.listdir(dirName)
    allFiles = list()
    # Iterate over all the entries
    for entry in listOfFile:
        # Create full path
        fullPath = os.path.join(dirName, entry)
        # If entry is a directory then get the list of files in this directory
        if os.path.isdir(fullPath):
            allFiles = allFiles + getListOfFiles(fullPath)
        else:
            allFiles.append(fullPath)
    return allFiles


def to_onehot(labels, max_label_num):
    assert type(labels) is np.ndarray
    labels_int = labels.astype(int)
    label_array = np.random.randint(-9, 9, size=(labels_int.size, max_label_num), dtype=np.int) * 0.01
    label_array[np.arange(labels_int.size), labels_int] = 1
    return label_array


def from_onehot(onehot_vec):
    """

    Args:
        onehot_vec: (row, cols) - Cols have the probability that the data belongs to that label

    Returns:
        the label
    """
    return np.argmax(onehot_vec)


def datagenerator(num_processes):
    images = []
    try:
        dirs_completed = os.listdir(image_load_path.replace('resized_crop', 'resized_crop_degraded'))
    except OSError:
        dirs_completed = []
        # os.mkdir(image_load_path.replace('resized_crop', 'resized_crop_degraded'))
    for img_load_dir in glob.iglob(image_load_path + '*/'):
        if img_load_dir[-6:-1] in dirs_completed:
            for img_path in glob.iglob(img_load_dir + '*.jpg'):
                if img_path.replace('resized_crop', 'resized_crop_degraded') in glob.iglob(
                        img_load_dir.replace('resized_crop', 'resized_crop_degraded') + '*.jpg'):
                    pass
                else:
                    if len(images) < num_processes:
                        images.append(img_path)
                    else:
                        yield images
                        images = []
        else:
            for img_path in glob.iglob(img_load_dir + '*.jpg'):
                if len(images) < num_processes:
                    images.append(img_path)
                else:
                    yield images
                    images = []


def change_br_cont(image, alpha=1.0, beta=0.0):
    if np.max(image) > 1:
        max_im = 255
    else:
        max_im = 1
    upper_lim = (0.5 * alpha + 0.5)
    upper_lim_actual = upper_lim * max_im
    lower_lim = (0.5 - 0.5 * alpha)
    lower_lim_actual = lower_lim * max_im
    diff = upper_lim - lower_lim
    image = (diff * image.astype(float) + lower_lim_actual)
    image = image + beta * diff * max_im
    image_2 = image + beta * diff * max_im
    if beta > 0:
        image[:, :, :2] = image_2[:, :, :2]
    else:
        image[:, :, 2] = image_2[:, :, 2]
    image[image < lower_lim_actual] = lower_lim_actual
    image[image > upper_lim_actual] = upper_lim_actual
    return image.astype(np.uint8)


def degrade_face_image(image_file_name):
    image_file_name = str(image_file_name)
    image_path = os.path.join(image_load_path, image_file_name)
    save_image_name = os.path.join(image_file_name.replace('resized_crop', 'resized_crop_degraded'))
    try:
        os.makedirs(os.path.dirname(save_image_name))
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(os.path.dirname(save_image_name)):
            pass
        else:
            raise
    rate = '20'
    outputdict = {
        '-vcodec': 'libx264',
        '-r': rate,
        '-pix_fmt': 'yuv420p',
        '-crf': '50'  # change for quality
    }
    # worst quality no more than 47
    tmp_video_file_name = save_image_name + '_tmp.mp4'
    writer = sio.FFmpegWriter(tmp_video_file_name, outputdict=outputdict)
    length = 20
    image = cv2.imread(image_path)
    if image is None:
        print(image_path)
        return
    image = cv2.cvtColor(cv2.resize(image, (256, 256)), cv2.COLOR_BGR2RGB)
    startx = 0
    starty = 0
    locs = []
    for i in range(length):
        locs.append((i, startx, starty))
        frame = np.zeros((800, 800, 3)).astype(np.uint8)
        frame[startx:startx + 256, starty:starty + 256, :] = image
        writer.writeFrame(frame)
        startx += np.random.randint(0, 8)
        starty += np.random.randint(0, 8)
        if startx + 256 > 800 or starty + 256 > 800:
            break
    writer.close()
    frames = sio.vread(tmp_video_file_name)
    os.remove(tmp_video_file_name)
    n, w, h, c = frames.shape
    frame_num = np.random.choice(n, 1)[0]
    i, x, y = locs[frame_num]
    degraded_image = frames[i, x:x + 256, y:y + 256]
    degraded_image = cv2.cvtColor(np.squeeze(degraded_image), cv2.COLOR_RGB2BGR)
    imwrite(save_image_name, degraded_image)



class Params():
    def __init__(self):
        self.epochs = None
        self.num_parallel_calls = None
        self.batch_size = None
        self.num_classes = None
        self.lr = None
        self.gpu = None


# if __name__ == '__main__':
#     with open('/media/soumya/HDD_2/Helen/annotation/2.txt') as f:
#         lines = f.read().splitlines()
#         image_name = '/media/soumya/HDD_2/Helen/train/' + lines[0] + '.jpg'
#         image = cv2.imread(image_name)
#         points = len(lines) - 1
#         for point in lines[1:]:
#             point = eval(point)
#             image[int(point[1]), int(point[0]), :] = 255
#         plt.imshow(image)
#         plt.show()

if __name__ == '__main__':
    import time

    num_process = 10
    p = Pool(num_process)
    aa = time.time()
    ii = 0
    jj = 0
    for img in datagenerator(num_process):
        bb = time.time()
        L = len(img)
        ii += L
        jj += L
        print('\r{}, {}'.format(ii, jj), end="")
        try:
            p.map(degrade_face_image, img)
        except Exception as e:
            logging.error(str(e))
            continue
        print(
            '\r{} images. Batch time {}. Total time {}. img/sec {}'.format(ii, time.time() - bb, time.time() - aa,
                                                                           jj / (time.time() - aa)), end="")
    print(time.time() - aa)