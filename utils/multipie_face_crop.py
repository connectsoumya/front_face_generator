# coding: utf-8
from typing import Any, Union

import mxnet as mx
from mini_pipeline.mtcnn_detector import MtcnnDetector
import numpy as np
import cv2
import os
from mini_pipeline.zen_rescnn_cm import ZenResCNNCMdeploy
import torch
from torch.autograd import Variable
from utils.misc import getListOfFiles
import logging
import logging.config
from configparser import ConfigParser
from tqdm import tqdm

parser = ConfigParser()
my_path = os.path.abspath(os.path.dirname(__file__))
parser.read('../config.ini')
logging.config.fileConfig('../logging.ini')
if not os.path.exists('../logger.log'):
    f = open('../logger.log', 'w+')
    f.close()

multipie_dir = parser.get('DATA', 'multipie_full')

def prepare_img(img, target_size=128):
    yy = int((img.shape[0] - target_size) / 2)
    xx = int((img.shape[1] - target_size) / 2)
    resized_img = img[yy: yy + target_size, xx: xx + target_size]

    result = np.zeros((1, 3, target_size, target_size), dtype=np.float32)

    # RGB data within [0,1]
    result[0, 2, :, :] = resized_img[:, :, 0]
    result[0, 1, :, :] = resized_img[:, :, 1]
    result[0, 0, :, :] = resized_img[:, :, 2]
    result = result / 255.0

    result = torch.from_numpy(result)
    result = Variable(result).cuda()

    return result


def load_weight(model, state_dict):
    '''
        load weight from state_dict to model, skip those with invalid shape
    :param model:
    :param state_dict:
    :return:
    '''
    for name, param in model.named_parameters():
        if not name in state_dict:
            continue
        if param.size() == state_dict[name].size():
            print('==> loading weight from ', name)
            param.data.copy_(state_dict[name])

# load detector
detector = MtcnnDetector(model_folder='../mini_pipeline/model', ctx=mx.gpu(0), num_worker=1, accurate_landmark=True)

# load feature extractor
checkpoint_file = '../mini_pipeline/zencnn_lm.pth'
model = ZenResCNNCMdeploy()
model.eval()
model.cuda()

# load state_dict
sd = torch.load(checkpoint_file)
sd = sd['state_dict']
sd = {k.partition('module.')[2]: v for k, v in sd.items()}
load_weight(model, sd)
print('-> FR model loaded')

# detect and align images
for (dirpath, dirnames, filenames) in os.walk(multipie_dir):
    for dirs in dirnames:
        dirs = os.path.join(dirpath, dirs)
        files_list = getListOfFiles(dirs)
        num_files = len(files_list)
        t = tqdm(total=num_files)
        for file in files_list:
            file = os.path.join(dirs, file)
            crop_file = file.replace('MultiPIE', 'MultiPIE_crop')
            par_dir = os.path.abspath(os.path.join(crop_file, os.pardir))
            if not os.path.exists(crop_file):
                img = cv2.imread(file)
                if img is None:
                    continue
                results = detector.detect_face(img)
                if results is not None:
                    total_boxes, points = results
                    # extract aligned face chips
                    chips = detector.extract_image_chips(img, points, 128, 0.37)
                    chip1 = chips[0]
                if not os.path.exists(par_dir):
                    os.makedirs(par_dir)
                cv2.imwrite(crop_file, chip1)
            t.update(n=1)
        t.close()
